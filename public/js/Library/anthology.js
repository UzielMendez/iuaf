$(document).ready(function() {
    $('#libraryPage').removeClass('activePage');
    $('#accountStatusPage').removeClass('activePage');
    $('#bankSubjectsPage').removeClass('activePage');
    $('#studentMaterialPage').removeClass('activePage');
    $('#exams').removeClass('activePage');
    $('#anthology').addClass('activePage');
    $('#qualification').removeClass('activePage');
    $('#schedule').removeClass('activePage');
    $('#generalData').removeClass('activePage');
});

function nameAnthologys() {
    let name = $('#nameAnthology').val();
    console.log(name);
}

function selectProgram() {
    let holis = $('#programAnthology').val();
    console.log(holis);
}

function clearModal() {
    $('#nameAnthology').val('');
    $('#programAnthology').val('0');
}

$(document).on('change', '#uploadAnthology', function(event) {
    var filename = $(this).val();
    console.log('file: ' + filename);
    if (filename == undefined || filename == "") {
        $(this).next('.custom-file-label').html('No se eligió archivo');
    } else {
        $(this).next('.custom-file-label').html(event.target.files[0].name);
    }
});


function deleteAnthology() {
    Swal.fire({
        title: '¿Deseas eliminar la antología?',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Si, eliminar!',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            Swal.fire(
                '¡Antología eliminada!',
                'La antología ha sido eliminada',
                'success'
            )
        }
    })
}