$(document).ready(function() {
    $('#libraryPage').addClass('activePage');
    $('#accountStatusPage').removeClass('activePage');
    $('#bankSubjectsPage').removeClass('activePage');
    $('#studentMaterialPage').removeClass('activePage');
    $('#exams').removeClass('activePage');
    $('#anthology').removeClass('activePage');
    $('#qualification').removeClass('activePage');
    $('#schedule').removeClass('activePage');
    $('#generalData').removeClass('activePage');

    var forms = document.getElementsByClassName("needs-validation");
    var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener(
            "submit",
            function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add("was-validated");
            },
            false
        );
    });

    $("#clearButton").click(clear);
});

function clear() {
    $("#searchLibrary").val("");
    $("#sendPetititon").submit();
}

$(document).on('change', '#uploadBook', function(event) {
    var filename = $(this).val();
    console.log('file: ' + filename);
    if (filename == undefined || filename == "") {
        $(this).next('.custom-file-label').html('No se eligió archivo');
    } else {
        $(this).next('.custom-file-label').html(event.target.files[0].name);
    }
});

function deleteBook(id) {
    Swal.fire({
        title: '¿Deseas eliminar el libro?',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: 'library/disabled/' + id,
                method: "PATCH",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                data: {
                    editBooks: id
                },
                success: function(data) {
                    if (data == 1) {
                        Swal.fire(
                            '¡Libro eliminado!',
                            'El libro ha sido eliminado correctamente',
                            'success'
                        );
                        setTimeout(() => {
                            return window.location.href = `library`;
                        }, 500)
                    }
                }
            });
        }

    })
}

function showBook(id) {
    $.ajax({
        url: `library/${id}/edit`,
        method: "GET",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        data: {
            editBooks: id
        },
        success: function(data) {
            var objBook = data[0];
            $('idBooks').val(objBook.id);
            $('#nameBooks').val(objBook.bookName);
            console.log(objBook[0]);
            $('#nameAuthors').val(objBook.bookAuthor);


            $('.upBook').html(objBook.bookFile);
            $(document).on('change', '#uploadBooks', function(event) {
                var filename = $(this).val();
                console.log('file: ' + filename);
                if (filename == undefined || filename == "") {
                    $(this).next('.custom-file-label').html('No se eligió archivo');
                } else {
                    $(this).next('.custom-file-label').html(event.target.files[0].name);
                }
            });

        }
    });
}