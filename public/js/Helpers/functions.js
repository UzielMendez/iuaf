function clean() {
    for (var i = 1; i <= 9; i++) {
        $("#semester option[value=" + i + "]").remove();
        $("#editSemester option[value=" + i + "]").remove();
    }
}

function programs(id) {

    if (id == "") {
        $("#semesterSelect").hide();
        clean();
    }
    if (id == "1") {
        clean();
        for (var i = 1; i <= 9; i++) {
            $("#semester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
            $("#editSemester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
        }
    }
    if (id == "2" || id == "3") {
        clean();
        for (var i = 1; i <= 5; i++) {
            $("#semester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
            $("#editSemester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
        }
    }
    if (id == "4") {
        clean();
        for (var i = 1; i <= 2; i++) {
            $("#semester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
            $("#editSemester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
        }
    }
    if (id == "5") {
        clean();
        for (var i = 1; i <= 1; i++) {
            $("#semester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
            $("#editSemester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
        }
    }
    if (id == "6") {
        clean();
        for (var i = 1; i <= 1; i++) {
            $("#semester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
            $("#editSemester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
        }
    }
}

function getValueForm(data, setInputName, setIputSelectId, attrHtml, attrHtmlSecondary) {
    var programSelected = null;
    var objProgram = data[0];

    $(`#${setInputName}`).val(objProgram.nameGroup);
    $(`#${setIputSelectId}`).val(objProgram.id);

    for (let i = 0; i < data[3].length; i++) {
        var optionProgram = $("<option />", {
            text: data[3][i].name,
            value: data[3][i].id,
            selected: false
        });

        if (data[3][i].name == objProgram.nameProgram) {

            optionProgram[0].selected = true;
            programSelected = data[3][i];
        }
        $(`#${attrHtml}`).prepend(optionProgram);
    }
    for (var j = 1; j <= programSelected.semesterNumber; j++) {
        var optionSemester = $("<option />", {
            text: j,
            value: j,
            selected: false
        });
        if (j == objProgram.semesterNumber) {
            optionSemester[0].selected = true;
        }
        $(`#${attrHtmlSecondary}`).prepend(optionSemester);
    }
}