$(document).ready(function() {
    $('#libraryPage').removeClass('activePage');
    $('#accountStatusPage').removeClass('activePage');
    $('#bankSubjectsPage').removeClass('activePage');
    $('#studentMaterialPage').addClass('activePage');
    $('#exams').removeClass('activePage');
    $('#anthology').removeClass('activePage');
    $('#qualification').removeClass('activePage');
    $('#schedule').removeClass('activePage');
    $('#generalData').removeClass('activePage');


    $("#quarter").hide();
    $("#matter").hide();
    $("#loadingSpinner").hide();
    $("#loadingSpinner2").hide();

    $("#cancelGroup").click(function() {
        $("#nameGroup").val("");
        $("#selectProgram").val("0");
        $("#selectQuarter").val("");
        $("#selectMatter").val("");
        $("#quarter").hide();
        $("#matter").hide();
    });

    $('#clearGroup').click(clear);


    $('#newSubjects').hide();
    $('#checkSubject').click(function() {
        if ($('#checkSubject').prop('checked')) {
            $('#newSubjects').show();
        } else {
            $('#newSubjects').hide();
        }
    });



    $('#buttonEditGroup').click(function() {
        var subjectsColumn = $('.subjectsTable tbody tr td');
    });

    $('.tableNewSubjects').on('click', '.hoverX', function() {
        $(this).prependTo('.subjectsTable').addClass('newSubjects');
        $('.subjectsTable tr td i').removeClass('d-none');

        let selectNewSubject = new Array();
        var subjectsColumn = $('.subjectsTable tbody tr .nS span');
        for (var i = 0; i < subjectsColumn.length; i++) {
            var valor = $(subjectsColumn[i]).text();
            selectNewSubject.push(valor);
        }
    });

    $('.subjectsTable').on('click', '.hoverX', function() {
        $(this).prependTo('.tableNewSubjects').removeClass('newSubjects');
        $('.tableNewSubjects tr td i').addClass('d-none');
    });

    $('#cancelEditGroup').click(function() {
        $('#editNameGroup').val('');
        $('#editSelectProgram').val('0');
        $('#editSelectQuarter').val('0');
        $('#checkSubject').prop('checked', false);
        $('#newSubjects').hide();
        $('#findsubjects').val('');
    });


    $('#loadingEditGroup').hide();

    $('#loadingNewSubject').hide();

    $('.messageResult').hide();
    let cards = document.getElementsByClassName('card').length;
    if (cards == 0) {
        $('.messageResult').show();
    } else {
        $('.messageResult').hide();
    }
});

function nameGroups() {
    var nameGroup = null;
    nameGroup = $("#nameGroup").val();
    $('#editNameGroup').val();
}

function selectPrograms(id) {
    if (id == "0") {
        $("#quarter").hide();
        $("#matter").hide();

        cleanVal();
    }
    if (id == "1") {
        loadSpinner();
        cleanVal();
        for (var i = 1; i <= 9; i++) {
            $("#selectQuarter").append(
                "<option value=" + i + ">" + i + "</option>"
            );
            $("#editSelectQuarter").append(
                "<option value=" + i + ">" + i + "</option>"
            );
        }
    }
    if (id == "2" || id == "3") {
        loadSpinner();
        cleanVal();
        for (var i = 1; i <= 5; i++) {
            $("#selectQuarter").append(
                "<option value=" + i + ">" + i + "</option>"
            );
            $("#editSelectQuarter").append(
                "<option value=" + i + ">" + i + "</option>"
            );
        }
    }
    if (id == "4") {
        loadSpinner();
        cleanVal();
        for (var i = 1; i <= 2; i++) {
            $("#selectQuarter").append(
                "<option value=" + i + ">" + i + "</option>"
            );
            $("#editSelectQuarter").append(
                "<option value=" + i + ">" + i + "</option>"
            );
        }
    }
    if (id == "5") {
        loadSpinner();
        cleanVal();
        for (var i = 1; i <= 1; i++) {
            $("#selectQuarter").append(
                "<option value=" + i + ">" + i + "</option>"
            );
            $("#editSelectQuarter").append(
                "<option value=" + i + ">" + i + "</option>"
            );
        }
    }
    if (id == "6") {
        loadSpinner();
        cleanVal();
        for (var i = 1; i <= 1; i++) {
            $("#selectQuarter").append(
                "<option value=" + i + ">" + i + "</option>"
            );
            $("#editSelectQuarter").append(
                "<option value=" + i + ">" + i + "</option>"
            );
        }
    }
    var programs = $("#selectProgram").val();
    var editPrograms = $("#editSelectProgram").val();
}

function loadSpinner() {
    $("#loadingSpinner").show();
    $("#quarter").hide();
    $("#matter").hide();
    setTimeout(function() {
        $("#quarter").show();
        $("#loadingSpinner").hide();
    }, 1000);
}

function cleanVal() {
    for (var i = 1; i <= 9; i++) {
        $("#selectQuarter option[value=" + i + "]").remove();
        $("#editSelectQuarter option[value=" + i + "]").remove();
    }
}

function selectQuarters(id) {
    loadingSpinner();
    var quarter = $("#selectQuarter").val();
    var editQuarter = $("#editSelectQuarter").val();
}

function selectQuartersModalAdd() {
    loadingSpinner();

    $('#matter').hide();
    var chooseProgram = $("#selectProgram").val();
    var editQuarter = $("#editSelectQuarter").val();
    $.ajax({

        url: `groups/${chooseProgram}/subject`,
        method: "GET",
        // headers: {
        //     "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        // },
        data: {
            groupId: chooseProgram
        },
        success: function(data) {

            $(`#selectMatter`).empty();
            $('#matter').show();
            for (let i = 0; i < data.length; i++) {
                var optionProgram = $("<option />", {
                    text: data[i].name,
                    value: data[i].id,
                    selected: false
                });
                $(`#selectMatter`).prepend(optionProgram);
            }
            $("#loadingSpinner2").hide();
        }
    });
}

function loadingSpinner() {
    $("#loadingSpinner2").show();
    setTimeout(function() {
        $("#matter").show();
        $("#loadingSpinner2").hide();
    }, 1000);
}

function selectMatters() {
    var matter = $("#selectMatter").val();
}

function clear() {
    $('#searchGroup').val("");
    $('.search').submit();
}

function deleteGroup(id) {
    Swal.fire({
        title: '¿Deseas eliminar el grupo?',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({

                url: 'groups/disabled/' + id,
                method: "PATCH",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                data: {
                    groupId: id
                },
                success: function(data) {
                    if (data == 1) {
                        Swal.fire(
                            '¡Grupo eliminado!',
                            'El grupo ha sido eliminado correctamente',
                            'success'
                        );
                        setTimeout(() => {
                            return window.location.href = `groups`;
                        }, 500)
                    }
                }
            });
        }

    })
}

function deleteSubjectGroup(idGroup, idSubject) {
    Swal.fire({
        title: '¿Deseas eliminar la materia de este grupo?',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({

                url: '/groups/subject/disabled/' + idSubject,
                method: "PATCH",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                data: {
                    groupId: idGroup
                },
                success: function(data) {
                    if (data == 1) {
                        Swal.fire(
                            '¡Materia eliminada!',
                            'La materia ha sido eliminada  del grupo',
                            'success'
                        );
                        setTimeout(() => {
                            return window.location.href = ``;
                        }, 500)
                    }
                }
            });
        }

    })
}