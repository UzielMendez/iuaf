$(document).ready(function () {
    $('#libraryPage').removeClass('activePage');
    $('#accountStatusPage').removeClass('activePage');
    $('#bankSubjectsPage').removeClass('activePage');
    $('#studentMaterialPage').addClass('activePage');
    $('#exams').removeClass('activePage');
    $('#anthology').removeClass('activePage');
    $('#qualification').removeClass('activePage');
    $('#schedule').removeClass('activePage');
    $('#generalData').removeClass('activePage');


    $('#mAddTeacher').click(function () {
        $('#containerListTeachers').hide();
    });

    $('#mAddAlumns').click(function () {
        $('#containerListAlumns').hide();
    });

    const def = 'NA';
    $('#openModalGrades').click(function () {
        if ($('#average').val() == '') {
            $('#average').val('NA');
        }
    });

    $('#clearAlumn').click(clearAlumns);
    $('#clearScore').click(clearScore);

    $('.loadingSearchTeacher').hide();


    let countTeacher = document.getElementsByClassName('countTeacher').length;
    if (countTeacher == 1) {
        $('#mAddTeacher').hide();
    } else {
        $('#mAddTeacher').show();
    }

    if ($('#searchNameAlumn').val() != '') {
        $('#sectionMaterial-tab').removeClass('active');
        $('#sectionMaterial').removeClass('show  active');
        $('#sectionAlumns-tab').addClass('active');
        $('#sectionAlumns').addClass('show active');
        $('#sectionScore-tab').removeClass('active');
        $('#sectionScore').removeClass('show  active');
    } else if ($('#searchScoreAlumn').val() != '') {
        $('#sectionMaterial-tab').removeClass('active');
        $('#sectionMaterial').removeClass('show  active');
        $('#sectionAlumns-tab').removeClass('active');
        $('#sectionAlumns').removeClass('show active');
        $('#sectionScore-tab').addClass('active');
        $('#sectionScore').addClass('show  active');
    }
});

function selectUser() {

    $('#tableListTeachers tbody tr td').on('click', function () {
        $('.test2').children().removeClass('nameSelected');
        let nameSelected = $(this).addClass('nameSelected').html();

        if (nameSelected == "") {
            $('#btnAddTeacher').attr("disabled");
        } else {
            $('#btnAddTeacher').removeAttr("disabled");
        }
    });

    $('#tableListAlumns tbody tr td').on('click', function () {
        $('.test2').children().removeClass('nameSelected');
        let nameSelected = $(this).addClass('nameSelected').html();

        if (nameSelected == "") {
            $('#btnAddAlumn').attr("disabled");
        } else {
            $('#btnAddAlumn').removeAttr("disabled");
        }
    });
}

function clearAlumns() {
    $('#searchNameAlumn').val('');
    $('#formSearchAlumn').submit();
}

function clearScore() {
    $('#searchScoreAlumn').val('');
    $('#formSearchScore').submit();
}

$(function () {

    $('.validnumbers').keypress(function (e) {
        if (isNaN(this.value + String.fromCharCode(e.charCode)))
            return false;
    })
        .on("cut copy paste", function (e) {
            e.preventDefault();
        });

});

function localiceUser(idGroup, txtSearch, typePeople, listPeopleInClass) {
    $.ajax({

        url: `/subjectsGroup/find/${idGroup}/user`,
        method: "GET",
        data: {
            text: txtSearch,
            category: typePeople

        },
        success: function (data) {
            if (data.length > 0) {
                $('#containerListTeachers').show();
                $('#containerListAlumns').show();
                $(`#${listPeopleInClass}`).empty();
                for (let i = 0; i < data.length; i++) {
                    var optionProgram =
                        `
                    <tr onclick="selectUser();" class="test2">
                        <td class="test3">${data[i].username}</td>
                    </tr>
                    `;
                    $(`#${listPeopleInClass}`).prepend(optionProgram);
                }
            }
        }
    });
}

function search(idGroup, typeUser) {

    let getNames = null;
    let getListNames = null;
    $('.loadingSearchTeacher').show();
    setTimeout(function () {
        $('.loadingSearchTeacher').hide();
        if (typeUser == 2) {
            getNames = $('#nameTeacher').val();
            if ($('#nameTeacher').val() == '') {
                $('#containerListTeachers').hide();
            } else {
                $('#containerListTeachers').show();
            }

            getListNames = "listStudent";
        } else {
            getNames = $('#nameAlumn').val();
            if ($('#nameAlumn').val() == '') {
                $('#containerListAlumns').hide();
            } else {
                $('#containerListAlumns').show();
            }
            getListNames = "listProfessor";
        }
        localiceUser(idGroup, getNames, typeUser, getListNames);
    }, 1000);
}

function agreePeopleInClass(groupId, subjectId, nameProgram, semester, type) {
    var getElementSelected = $('.nameSelected').text();
    $.ajax({

        url: `/subjectsGroup/agree/user`,
        method: "POST",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        data: {
            userInClass: getElementSelected,
            groupId,
            subjectId,
            nameProgram,
            semester
        },
        success: function (data) {
            if (data != 0) {
                Swal.fire(
                    `¡${type} agregado!`,
                    `El ${type} ha sido agregado correctamente`,
                    'success'
                );
                setTimeout(() => {
                    return window.location.href = ``;
                }, 500)
            }
        }
    });
}

function clearModal() {
    $('#nameTeacher').val('');
    $('#nameAlumn').val('');
}

function deleteAlumn(id, groupId, subjectId, type) {
    var typeUser = 'Alumno';
    if (type == 'teacher') {
        typeUser = "Profesor";
    }
    Swal.fire({
        title: `¿Deseas eliminar al ${typeUser}?`,
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Eliminar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '/subjectsGroup/remove/people',
                method: `PATCH`,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                data: {
                    peopleInClass: id,
                    groupId,
                    subjectId
                },
                success: function (data) {
                    if (data == 1) {
                        Swal.fire(
                            `¡${typeUser} eliminado!`,
                            `El ${typeUser} ha sido eliminado correctamente`,
                            'success'
                        )
                    }
                    setTimeout(() => {
                        return window.location.href = ``;
                    }, 500);
                }
            });
        }
    })
}

function getQualitification(id) {
    $.ajax({

        url: `/subjectsGroup/subjects/edit/qualification`,
        method: "GET",
        data: {
            qualificationId: id
        },
        success: function (data) {
            if (data != null || data != undefined) {
                $('#idQualification').val(data.id)
                $('#assists').val(data.assists)
                $('#average').val(data.average)
            }
        }
    });
}

function loaderEdit(id) {
    $('#loadingEdit').show();
    $('.modal-contentGroup').addClass('transparent');
    $('#modal-bodyGroup').hide();
    $.ajax({

        url: `bankMatters/${id}/edit`,
        method: "GET",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        data: {
            subjectId: id
        },
        success: function (data) {
            if (data != null || data != undefined) {
                $('#loadingEdit').hide();
                $('.modal-content').removeClass('transparent');
                $('#modal-body').show();
                var objSubject = data[0];
                var programSelected = null;
                $('#editNameSubject').val(objSubject.name);
                $('#idSubject').val(objSubject.id);

                for (let i = 0; i < data[1].length; i++) {
                    var optionProgram = $("<option />", {
                        text: data[1][i].name,
                        value: data[1][i].id,
                        selected: false
                    });
                    if (data[1][i].name == objSubject.nameProgram) {
                        optionProgram[0].selected = true;
                        programSelected = data[1][i];
                    }
                    $(`#editProgram`).prepend(optionProgram);
                }

                for (var j = 1; j <= programSelected.semesterNumber; j++) {
                    var optionSemester = $("<option />", {
                        text: j,
                        value: j,
                        selected: false
                    });
                    if (j == objSubject.semesterNumber) {
                        optionSemester[0].selected = true;
                    }
                    $(`#editSemester`).prepend(optionSemester);
                }
            }
        }
    });
}

function updateQualification(id) {
    let idQualification = $('#idQualification').val();

    $.ajax({

        url: `/subjectsGroup/subjects/edit/${idQualification}/qualification`,
        method: "PATCH",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        data: {
            assists: $('#assists').val(),
            average: $('#average').val(),
        },
        success: function (data) {

            if (data == 1) {
                Swal.fire(
                    '¡Calificación actualizada!',
                    'La información ha sido actualizada correctamente',
                    'success'
                );
                return window.location.href = ``;
            } else {
                Swal.fire(
                    'Calificación no  actualizada!',
                    'Al parecer ocurrió  un problema intentalo más tarde',
                    'error'
                );
                return window.location.href = ``;
            }
        }
    });
}

function downloadExcelQualification(idG, idS) {
    var getFilter = $("#searchScoreAlumn").val();
    var params = null;

    if (getFilter != '') {
        params = getFilter;
    }
    window.location.href = `/subjectsGroup/export/qualifications/subject/${idS}/group/${idG}/${params}`;
}