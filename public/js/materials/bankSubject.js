$(document).ready(function() {
    $('#libraryPage').removeClass('activePage');
    $('#accountStatusPage').removeClass('activePage');
    $('#bankSubjectsPage').addClass('activePage');
    $('#studentMaterialPage').removeClass('activePage');
    $('#exams').removeClass('activePage');
    $('#anthology').removeClass('activePage');
    $('#qualification').removeClass('activePage');
    $('#schedule').removeClass('activePage');
    $('#generalData').removeClass('activePage');

    $('.cancelSubject').click(function() {
        $('#semester').val('');
        $('#program').val('0');
        $('#nameSubject').val('');
    });

    $('#clearSubject').click(clearSubjects);

    $('#loadingEdit').hide();

});

function clearSubjects() {
    $('#searchSubject').val("");
    $('.formSearchSubject').submit();
}

function subject() {
    var nameSubject = $('#nameSubject').val();
    var editNameSubject = $('#editNameSubject').val();
}

function programs(id) {

    if (id == "") {
        $("#semesterSelect").hide();
        clean();
    }
    if (id == "1") {
        clean();
        for (var i = 1; i <= 9; i++) {
            $("#semester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
            $("#editSemester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
        }
    }
    if (id == "2" || id == "3") {
        clean();
        for (var i = 1; i <= 5; i++) {
            $("#semester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
            $("#editSemester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
        }
    }
    if (id == "4") {
        clean();
        for (var i = 1; i <= 2; i++) {
            $("#semester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
            $("#editSemester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
        }
    }
    if (id == "5") {
        clean();
        for (var i = 1; i <= 1; i++) {
            $("#semester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
            $("#editSemester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
        }
    }
    if (id == "6") {
        clean();
        for (var i = 1; i <= 1; i++) {
            $("#semester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
            $("#editSemester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
        }
    }

    var programSelected = $('#program').val();
    var EditProgramSelected = $('#editProgram').val();
}

function selectSemester() {
    var selectSemester = $('#semester').val();
    var editSelectSemester = $('#editSemester').val();
}

function clean() {
    for (var i = 1; i <= 9; i++) {
        $("#semester option[value=" + i + "]").remove();
        $("#editSemester option[value=" + i + "]").remove();
    }
}

function deleteSubject(id) {
    Swal.fire({
        title: '¿Deseas eliminar la materia?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: 'bankMatters/disabled/' + id,
                method: "PATCH",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                data: {
                    subjectId: id
                },
                success: function(data) {
                    if (data == 1) {
                        Swal.fire(
                            '¡Grupo eliminado!',
                            'La materia ha sido eliminada correctamente',
                            'success'
                        );
                        setTimeout(() => {
                            return window.location.href = `bankMatters`;
                        }, 500)
                    }
                }
            });
        }
    })
}




function loaderEdit(id) {
    $('#loadingEdit').show();
    $('.modal-contentGroup').addClass('transparent');
    $('#modal-bodyGroup').hide();
    $.ajax({

        url: `bankMatters/${id}/edit`,
        method: "GET",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        data: {
            subjectId: id
        },
        success: function(data) {
            if (data != null || data != undefined) {
                $('#loadingEdit').hide();
                $('.modal-content').removeClass('transparent');
                $('#modal-body').show();
                var objSubject = data[0];
                var programSelected = null;
                $('#editNameSubject').val(objSubject.name);
                $('#idSubject').val(objSubject.id);

                for (let i = 0; i < data[1].length; i++) {
                    var optionProgram = $("<option />", {
                        text: data[1][i].name,
                        value: data[1][i].id,
                        selected: false
                    });
                    if (data[1][i].name == objSubject.nameProgram) {
                        optionProgram[0].selected = true;
                        programSelected = data[1][i];
                    }
                    $(`#editProgram`).prepend(optionProgram);
                }

                for (var j = 1; j <= programSelected.semesterNumber; j++) {
                    var optionSemester = $("<option />", {
                        text: j,
                        value: j,
                        selected: false
                    });
                    if (j == objSubject.semesterNumber) {
                        optionSemester[0].selected = true;
                    }
                    $(`#editSemester`).prepend(optionSemester);
                }
            }
        }
    });
}

function validateInputs() {
    let name = $('#editNameSubject').val();
    if ($('#editProgram').val() == 0) {
        return false;
    } else if (name == '' || name == ' ') {
        return false;
    } else if ($('#editSemester').val() == 0) {
        return false;
    }
    return true;
}

function updateSubject() {

    let verifyInputs = validateInputs();
    if (!verifyInputs) {
        Swal.fire({
            icon: 'Error',
            title: 'Lo siento...',
            text: 'No puedes dejar ningun campo vacío o escoge una opción válida.',
        });
        return false;
    }

    $('#loadingEdit').show();
    $('.modal-content').addClass('transparent');
    $('#modal-body').hide();

    let id = $('#idSubject').val();
    let name = $('#editNameSubject').val();
    let program = $('#editProgram').val();
    let semester = $('#editSemester').val();

    $.ajax({
        url: `bankMatters/${id}`,
        method: "PATCH",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        data: {
            name,
            program,
            semester,
        },
        success: function(data) {
            if (data == 1) {
                Swal.fire(
                    '¡Grupo actualizado!',
                    'La materia ha sido actualizada correctamente',
                    'success'
                );
                return window.location.href = `bankMatters`;
            } else {
                Swal.fire(
                    'Materia no actualizada!',
                    'Al parecer ya tienes otra materia registrada con ese nombre, prueba con otro',
                    'error'
                );
                return window.location.href = `bankMatters`;
            }
        }
    });
}