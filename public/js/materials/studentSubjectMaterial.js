function getSubjectGroup(id) {
    $('#loadingEditGroup').show();
    $('#modal-contentGroup').addClass('transparentGroups');
    $('#modal-bodyGroup').hide();
    $.ajax({

        url: `groups/${id}/edit`,
        method: "GET",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        data: {
            groupId: id
        },
        success: function(data) {
            if (data != null || data != undefined) {
                $('#loadingEditGroup').hide();
                $('#modal-contentGroup').removeClass('transparentGroups');
                $('#modal-bodyGroup').show();
                $('#editNameGroup').val(data[0].nameGroup);
                getValueForm(data, "editNameSubject", "idGroup", "editSelectProgram", "editSelectQuarter");

                let mattersInGroup = data[1];
                if (mattersInGroup.length > 0) {
                    $('#matters').empty();
                    mattersInGroup.forEach(item => {
                        let columnMatter =
                            `
                    <tr class="subjectOn">
                       <td>${item.name}</td>
                    </tr>
                    `
                        $('#matters').append(columnMatter);
                    });
                }
                let subjects = 0;
                subjects = document.getElementsByClassName('subjectOn').length;
                if (subjects == 0) {
                    $('.messageResultTable').show();
                    $('#subjectsTable').hide();
                } else {
                    $('.messageResultTable').hide();
                    $('#subjectsTable').show();
                }
                let listmattersProgram = data[2];
                if (listmattersProgram == '') {
                    listmattersProgram.length == 0;
                }
                console.log(listmattersProgram.length);
                if (listmattersProgram.length > 0) {
                    $('#listAllMatters').empty();
                    listmattersProgram.forEach(item => {
                        let columnMatter =
                            `
                    <tr class="hoverX">
                        <td  id= "${item.id}" class="nS"><span>${item.name}</span> 
                            <i class="deleteSubject fas fa-times text-danger d-none"></i>
                        </td>
                    </tr>
                    `
                        $('#listAllMatters').append(columnMatter);
                    });
                }
                if (listmattersProgram.length === 0) {
                    $('#tableNewSubjects').hide();
                    $('#emptyMessage').removeClass('d-none');
                }
            }
        }
    });
}

function findSubject() {
    var nameSubject = $('#findsubjects').val();
    $('#loadingNewSubject').show();
    $('#emptyMessage').addClass('d-none');
    $('#tableNewSubjects').hide();
    var idProgram = $("#editSelectProgram").val();

    let listSubjects = [];
    var getListSubject = $("#matters").children();

    for (let i = 0; i < getListSubject.length; i++) {
        var subject = getListSubject[i].children[0].textContent;
        if (subject.includes("\n")) {
            let findIndex = subject.indexOf('\n');
            subject = subject.substring(0, findIndex).trim();
        }
        listSubjects.push(subject);
    }

    $.ajax({

        url: `groups/find/subject`,
        method: "GET",
        data: {
            subject: nameSubject,
            idProgram,
            listSubjects
        },
        success: function(data) {
            if (data != null || data != undefined) {
                if (data.length === 0) {
                    $('#emptyMessage').removeClass('d-none');
                    $('#loadingNewSubject').hide();
                    $('#tableNewSubjects').hide();
                } else {
                    $('#emptyMessage').addClass('d-none')
                    $('#loadingNewSubject').hide();
                    $('#tableNewSubjects').show();
                }
                $('#listAllMatters').empty();
                for (let i = 0; i < data.length; i++) {

                    let columnSubject =
                        `
              <tr class="hoverX newSubjects">
                 <td class="nS">
                 <span>${data[i].name}</span>
                 <i class="deleteSubject fas fa-times text-danger d-none"></i>
                 </td>
              </tr>
              `
                    $('#listAllMatters').append(columnSubject);
                }
            }
        }
    });
}

function updateGroup() {
    let id = $('#idGroup').val();
    let name = $('#editNameGroup').val();
    let program = $('#editSelectProgram').val();
    let semester = $('#editSelectQuarter').val();
    let listSubjects = [];
    var getListSubject = $("#matters").children();

    for (let i = 0; i < getListSubject.length; i++) {
        var subject = getListSubject[i].children[0].textContent;
        if (subject.includes("\n")) {
            let findIndex = subject.indexOf('\n');
            subject = subject.substring(0, findIndex).trim();
        }
        listSubjects.push(subject);
    }
    $.ajax({

        url: `groups/${id}`,
        method: "PATCH",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        data: {
            name,
            program,
            semester,
            listSubjects
        },
        success: function(data) {
            console.log(data);

            if (data == 1) {
                Swal.fire(
                    '¡Grupo actualizado!',
                    'El grupo ha sido actualizado correctamente',
                    'success'
                );
                setTimeout(() => {
                    return window.location.href = `groups`;
                }, 800);

            } else {
                Swal.fire(
                    'Grupo no actualizado!',
                    'Al parecer ocurrió un error intentalo más tarde',
                    'error'
                );
                setTimeout(() => {
                    return window.location.href = `groups`;
                }, 1500);
            }
        }
    });
}