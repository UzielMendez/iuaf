$(document).ready(function() {
    $('#qualification').addClass('activePage');

    $('#searchNameAlumn').hide();
    $('#searchProgramAlumn').hide();
    $('#searchSubjectAlumn').hide();
    $('#btnSearchQualification').hide();
    $('#formSearchQualification').addClass('d-flex');

    if ($('#searchNameAlumn').val() != '') {
        $('#searchNameAlumn').show();
        $('#btnSearchQualification').show();
        $('#formSearchQualification').removeClass('d-flex');
    } else if ($('#searchProgramAlumn').val() != '') {
        $('#searchProgramAlumn').show();
        $('#btnSearchQualification').show();
        $('#formSearchQualification').removeClass('d-flex');
    } else if ($('#searchSubjectAlumn').val() != '') {
        $('#searchSubjectAlumn').show();
        $('#btnSearchQualification').show();
        $('#formSearchQualification').removeClass('d-flex');
    }

    (function() {
        'use strict';
        window.addEventListener('load', function() {
            var forms = document.getElementsByClassName('needs-validation');
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
});

function selectFilter() {
    if ($('#optionsQualification').val() == '') {
        $('#searchNameAlumn').hide();
        $('#searchProgramAlumn').hide();
        $('#searchSubjectAlumn').hide();
        $('#btnSearchQualification').hide();
        $('#formSearchQualification').addClass('d-flex');
    } else if ($('#optionsQualification').val() == 'name') {
        $('#searchNameAlumn').show();
        $('#searchProgramAlumn').hide();
        $('#searchSubjectAlumn').hide();
        $('#btnSearchQualification').show();
        $('#formSearchQualification').removeClass('d-flex');
    } else if ($('#optionsQualification').val() == 'program') {
        $('#searchNameAlumn').hide();
        $('#searchProgramAlumn').show();
        $('#searchSubjectAlumn').hide();
        $('#btnSearchQualification').show();
        $('#formSearchQualification').removeClass('d-flex');
    } else if ($('#optionsQualification').val() == 'subject') {
        $('#searchNameAlumn').hide();
        $('#searchProgramAlumn').hide();
        $('#searchSubjectAlumn').show();
        $('#btnSearchQualification').show();
        $('#formSearchQualification').removeClass('d-flex');
    }
}

function btnReset() {
    $('#searchNameAlumn').val('');
    $('#searchProgramAlumn').val('');
    $('#searchSubjectAlumn').val('');
    $('#btnSearchQualification').hide();
    $('#optionsQualification').val('0');
    $('#clearScore').hide();
}

function clearModal() {
    $('#averageQ').val('');
    $('#semesterQ').val('');
}


function downloadExcelQualifications() {
    var getType = $('#optionsQualification').val();
    var params = null;
    var typeParams = null;

    if (getType == '' || getType == ' ') {
        getType = null
    }
    if (getType == 'name') {
        typeParams = 'searchNameAlumn'
    } else if (getType == 'program') {
        typeParams = 'searchProgramAlumn'
    } else if (getType == 'subject') {
        typeParams = 'searchSubjectAlumn'
    }

    if (typeParams != '') {
        params = $(`#${typeParams}`).val();
    }

    window.location.href = `/qualifications/export/type/${getType}/filter/${params}`;
}