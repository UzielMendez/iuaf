@extends('layouts.app')

@section('content')
    <div class="container-login">
        <div class="container col">
            <div class="col-auto center">
                <form method="POST" action="{{ route('login') }}"
                    class="d-flex flex-wrap flex-row justify-content-center bg-light p-4 rounded-3 ajust">
                    @csrf
                    <div class="d-flex flex-wrap justify-content-center">
                        <img src="images/IUAF.png" alt="IUAF" class="w-50">

                        <div class="form-group col-12 mb-3">
                            <label for="login">{{ __('Usuario') }}</label>
                            <div>
                            <input id="login" type="text" class="form-control @if ($errors->has('username') || $errors->has('email')) is-invalid @endif" name="login" value="{{ old('username') ?: old('email') }}" require autocomplete="login" autofocus placeholder="Ingresa tu usuario...">
                            @if ($errors->has('username') || $errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('username') ?: $errors->first('email') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group col-12 mb-3">
                            <label for="password">{{ __('Contraseña') }}</label>

                            <div>
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password" required
                                    autocomplete="current-password" placeholder="Ingresa tu contraseña...">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group ">
                            <div class="form-check">
                                <input class="form-check-input text-left" type="checkbox" name="remember" id="remember"
                                    {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Recuérdame') }}
                                </label>
                            </div>
                        </div>

                        <div class="form-group col-12 text-center">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Iniciar sesión') }}
                            </button>

                            @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('¿No tienes cuenta?, ¡Contáctanos!') }}
                                </a>
                            @endif
                        </div>
                </form>
            </div>
        </div>
    </div>
@endsection
