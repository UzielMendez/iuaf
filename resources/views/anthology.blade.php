@extends('layouts.dashboard')

@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{ asset('js/Home/home.js') }}" ></script> 
<script src="{{ asset('js/Library/anthology.js') }}"></script>
@include('popper::assets')
@endsection

@section('home')
    <div id="anthologys" class="bg-white w-100 h-100">
        <div class="containerSearch">
            <form id="formSearchAnthology" action="">
                <input class="form-control" name="find" type="text" placeholder="Buscar Antología...">
                <button class="btn"><i class="fas fa-search"></i></button>
            </form>
            @if (Auth::user()->role == '1')
            <div class="buttonAddAnthology">
                <button id="mAddAnthology" href="#" data-toggle="modal" data-target="#addAnthology" class="btn"><i class="fas fa-plus"></i>Subir Antología</button>
            </div>
            @endif
            <div id="modalAddAnthology">
                <div class="modal fade" id="addAnthology" tabindex="-1" role="dialog"
                                aria-labelledby="addAnthologyLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered justify-content-center" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <h5>Agregar Antología</h5>
                                            <form action="">
                                                <div class="form-group">
                                                    <label for="nameAnthology">Nombre de la antología</label>
                                                    <input id="nameAnthology" type="text"
                                                        placeholder="Escribe el nombre de la antología..." class="form-control" onkeyup="nameAnthologys();">
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label">Programa</label>
                                                    <select name="" id="programAnthology" class="form-control" onchange="selectProgram();">
                                                        <option value="0">Selecciona un programa...</option>
                                                        <option value="1">opcion 1</option>
                                                        <option value="2">opcion 2</option>
                                                    </select>
                                                </div>
                                                <div class="input-group mb-3 mt-3 flex-column">
                                                    <label class="labelUploadAnthology">Elige tu archivo</label>
                                                    <div class="custom-file w-100">
                                                        <input type="file" class="custom-file-input" id="uploadAnthology">
                                                        <label class="upAnthology custom-file-label" for="uploadAnthology">Seleccionar archivo...</label>
                                                    </div>
                                                </div>
                                                <div class="btns">
                                                    <button onclick="clearModal();" class="btn"
                                                        data-dismiss="modal">Cancelar</button>
                                                    <button id="btnAddTeacher" type="submit" class="btn">Agregar</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
            </div>
        </div>
        <div id="containerAnthologys">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td>ID</td>
                        <td>Nombre</td>
                        <td>Autor</td>
                        <td>Programa</td>
                        <td>Acciones</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>654324</td>
                        <td>Antología 1</td>
                        <td>IUAF</td>
                        <td>Licenciatura en Derecho</td>
                        <td>
                            <i {{ Popper::arrow()->pop('Visualizar') }} class="fas fa-eye"></i>
                            @if(Auth::user()->role == '1')
                            <i {{ Popper::arrow()->pop('Editar') }} class="fas fa-pencil-alt"></i>
                            <i {{ Popper::arrow()->pop('Eliminar') }} class="fas fa-trash-alt" onclick="deleteAnthology();"></i>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection