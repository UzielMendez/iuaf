@extends('layouts.dashboard')

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ asset('js/Home/home.js') }}"></script>
    <script src="{{ asset('js/Library/filterLibrary.js') }}"></script>
    <script src="{{ asset('js/Accounts/accounts.js') }}"></script>
    @include('popper::assets')
@endsection

@section('home')
    <div id="accounts" class="library w-100 h-100">
        <div class="container">
            @if ($roleActual->role_id == 2)
                <div id="AddPayment" class="d-flex justify-content-between">
                    <a href="https://drive.google.com/file/d/1coGHuaIlAFOzhgjAHTcGvAG1OCRqcRtR/view" target="_blanck"
                        class="d-flex howPay align-items-center">
                        <i class="far fa-question-circle"></i>
                        <p>¿Cómo pagar?</p>
                    </a>
                    <button id='testClick' type="button" class="btn box-button mb-3" data-toggle="modal"
                        data-target="#exampleModalCenter">CAPTURA TU RECIBO</button>
                </div>

                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <form class="form-register">
                                <div class="form-register__header">
                                    <ul class="progressbar">
                                        <li id="step1" class="progressbar__option active"></li>
                                        <li id="step2" class="progressbar__option"></li>
                                        <li id="step3" class="progressbar__option"></li>
                                    </ul>
                                </div>
                                <div class="form-register__body">
                                    <div class="step active" id="step-1">
                                        <div class="step__header">
                                            <h2>Datos personales</h2>
                                        </div>
                                        <div class="step__body">
                                            <form id="steps-form" class="needs-validation" novalidate>
                                                <div id="PersonInformation" class="form-inline col-sm-12 mt-3">
                                                    <label for="fullName" class="control-label col-sm-12">Nombre
                                                        completo*</label>
                                                    <input id="fullName" name="nameFull" type="text"
                                                        class="form-control col-sm-12" required
                                                        onkeyup="return passValue(this);">
                                                    <div class="noInvalid d-none text-danger">Escribe tu nombre.
                                                    </div>
                                                </div>

                                                <div class="form-inline col-sm-12 mt-3">
                                                    <label for="registrationInternal"
                                                        class="control-label col-sm-12">Matricula interna*</label>
                                                    <input id="registrationInternal" class="form-control col-sm-12"
                                                        type="text" required onkeyup="return passValue(this);">
                                                    <div class="noInvalid d-none text-danger">Escribe la matrícula
                                                        interna.</div>
                                                </div>

                                                <div class="form-inline col-sm-12 mt-3">
                                                    <label for="autorizationNumber" class="control-label col-sm-12">No.
                                                        Autorización*</label>
                                                    <input id="autorizationNumber" required class="form-control col-sm-12"
                                                        type="number" onkeyup="return passValue(this); ValNumber(this);">
                                                    <div class="noInvalid d-none text-danger">Escribe el numero de
                                                        autorización.</div>
                                                </div>
                                                <p id="requiredFields">Todos los campos son
                                                    obligatorios*</p>
                                                <div class="step__footer">
                                                    <button id="next-1" type="button"
                                                        class="step__button step__button--next" data-to-step="2"
                                                        data-step="1">Siguiente</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <div class="step d-none" id="step-2">
                                        <div class="step__header">
                                            <h2>Datos del recibo</h2>
                                        </div>
                                        <div class="step__body">
                                            <form class="ml-0">
                                                <div class="form-inline col-sm-12 mt-3">
                                                    <label for="payAmount" class="control-label col-sm-12 mb-2">Monto del
                                                        pago</label>
                                                    <input id="payAmount" class="form-control col-sm-12" type="number"
                                                        required onkeyup="return passValue(this); ValNumber(this)">
                                                </div>
                                                <div class="form-inline col-sm-12">
                                                    <label for="methodPayment"
                                                        class="control-label col-sm-12 mb-2 mt-4">Método de pago</label>
                                                    <select id="methodPayment" class="form-control w-100" required
                                                        onchange="return passValue(this);">
                                                        <option value="">Elija la opción...</option>
                                                        <option value="Efectivo">EFECTIVO (Cajero inteligente Banamex)
                                                        </option>
                                                        <option value="BANCO">BANCO</option>
                                                        <option value="SPEI">SPEI</option>
                                                        <option value="CLIP">CLIP</option>
                                                        <option value="CHEDRAUI">CHEDRAUI</option>
                                                    </select>
                                                </div>

                                                <div class="form-inline col-sm-12">
                                                    <label for="conceptPay"
                                                        class="control-label col-sm-12 mb-2 mt-4">Concepto de pago</label>
                                                    <select id="conceptPay" class="form-control w-100" required
                                                        onchange="return passValue(this);">
                                                        <option value="">Elija la opción...</option>
                                                        <option value="Colegiatura">Colegiatura</option>
                                                        <option value="Inscripción">Inscripción</option>
                                                        <option value="Reinscripción">Reinscripción</option>
                                                        <option value="Abono a titulación">Abono a titulación</option>
                                                        <option value="Constancia">Constancia</option>
                                                        <option value="Certificado parcial">Certificado parcial</option>
                                                    </select>
                                                </div>

                                                <div class="form-inline col-sm-12">
                                                    <label for="paymentProof"
                                                        class="control-label col-sm-12 mb-2 mt-4">Comprobante de
                                                        pago</label>
                                                    <select id="paymentProof" class="form-control w-100" required
                                                        onchange="return passValue(this);">
                                                        <option value="">Elija la opción...</option>
                                                        <option value="Recibo">Recibo</option>
                                                        <option value="Factura">Factura</option>
                                                    </select>
                                                </div>
                                                <div class="step__footer">
                                                    <button id="back-1" type="button"
                                                        class="step__button step__button--back" data-to-step="1"
                                                        data-step="2">Regresar</button>
                                                    <button id="next-2" type="button"
                                                        class="step__button step__button--next" data-to-step="3"
                                                        data-step="2">Siguiente</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <div class="step d-none" id="step-3">
                                        <div class="step__header">
                                            <h2>Recopilación de datos</h2>
                                        </div>
                                        <div class="step__body">
                                            <form id="dataCollection"
                                                class="form-inline col-sm-12 mt-3 ml-0 border p-0 rounded">
                                                <label class="col-sm-12 p-0 rounded-top">Nombre</label>
                                                <input id="nameF" name="nameF" disabled
                                                    class="col-sm-12 border-0 font-weight-bold text-center">

                                                <label class="col-sm-12 p-0">Matrícula interna</label>
                                                <input id="internalR" name="internalE" type="text" disabled
                                                    class="col-sm-12 border-0 font-weight-bold text-center">

                                                <label class="col-sm-12 p-0">No. Autorización</label>
                                                <input id="noAutorization" name="noAutorization" type="text" disabled
                                                    class="col-sm-12 border-0 font-weight-bold text-center">

                                                <label class="col-sm-12 p-0">Monto del pago</label>
                                                <input id="paymentA" name="paymentA" type="text" disabled
                                                    class="col-sm-12 border-0 font-weight-bold text-center">

                                                <label class="col-sm-12 p-0">Método de pago</label>
                                                <input id="paymentM" name="paymentM" type="text" disabled
                                                    class="col-sm-12 border-0 font-weight-bold text-center">

                                                <label class="col-sm-12 p-0">Concepto de
                                                    pago</label>
                                                <input id="payC" name="payC" type="text" disabled
                                                    class="col-sm-12 border-0 font-weight-bold text-center">

                                                <label class="col-sm-12 p-0">Comprobante de pago</label>
                                                <input id="proofPay" name="proofPay" type="text" disabled
                                                    class="col-sm-12 border-0 font-weight-bold text-center">

                                                <div class="step__footer">
                                                    <button id="back-2" type="button"
                                                        class="step__button step__button--back" data-to-step="1"
                                                        data-step="2">Regresar</button>
                                                    <button id="finish" onclick="agreeAccount();" type="submit"
                                                        class="step__button step__button--next" data-to-step="3"
                                                        data-step="2">Finalizar</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endif
            @if ($roleActual->role_id == 1)
                <div id="AddPayment" class="d-flex justify-content-between">
                    <button onclick="return downloadExcel();" id="conciliate" class="btn" type="button">
                        <i class="far fa-file-excel"></i>
                        Descargar conciliación
                    </button>
                    <a id="testu" href="#"></a>
                </div>

                <div id="filterPayment">
                    <form id="optionsFilter" method="GET">
                        <select name="options" id="options" class="form-control" onchange="show(this.value);">
                            <option value="">Filtrar por...</option>
                            <option id="valName" value="name">Nombre</option>
                            <option id="valDate" value="date">Fecha</option>
                            <option id="valAuto" value="auto">Autorización</option>
                        </select>
                        <label id="searchNameLabel" for="searchFilterName">Nombre alumno</label>
                        <input value="{{ $filterName }}" id="searchFilterNames" name="searchFilterName" type="text"
                            class="form-control bg-white" placeholder="Escribe el nombre del alumno...">

                        <label id="filterFromLabel" for="filterFrom">Desde: </label>
                        <input value="{{ $filterFrom }}" id="filterFrom" name="filterFrom" type="date"
                            class="form-control bg-white mr-3">

                        <label id="filterToLabel" for="filterTo">Hasta: </label>
                        <input value="{{ $filterTo }}" id="filterTo" name="filterTo" type="date"
                            class="form-control bg-white">

                        <label id="filterAutorizationLabel" for="filterAuto">No Autorización: </label>
                        <input value="{{ $filterAuto }}" id="filterAutorization" name="filterAuto" type="number"
                            class="form-control bg-white" placeholder="Escribe el numero de autorización">

                        <button id="filterSearch" type="submit" onclick="filter();">Buscar</button>
                        @if ($showClearFilter)
                            <button id="filterClearButton" {{ Popper::arrow()->pop('Eliminar') }} type="button"
                                class="btn btnSearch"><i class="fas fa-trash-alt"></i></button>
                        @endif
                    </form>
                </div>
                <div class="books h-100 w-100 text-center">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td>Id</td>
                                <td>Nombre del Alumno</td>
                                <td>Autorización</td>
                                <td>Concepto Pago</td>
                                <td>Monto pago</td>
                                <td>Fecha pago</td>
                                <td>Status</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($listAcount as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->nameComplete }}</td>
                                    <td>{{ $item->noAutorization }}</td>
                                    <td>{{ $item->proofOfPayment }}</td>
                                    <td>{{ $item->paymentAmount }}</td>
                                    <td>{{ $item->payDay }}</td>
                                    <td>{{ $item->status }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <div class="books h-100 w-100 text-center mt-5">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td>Id</td>
                                <td>Autorización</td>
                                <td>Concepto Pago</td>
                                <td>Monto pago</td>
                                <td>Fecha pago</td>
                                <td>Status</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($listAcount as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->noAutorization }}</td>
                                    <td>{{ $item->proofOfPayment }}</td>
                                    <td>{{ $item->paymentAmount }}</td>
                                    <td>{{ $item->payDay }}</td>
                                    <td>{{ $item->status }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
            <div class="page">
                {{ $listAcount->appends(['find' => $roleActual])->links('pagination::bootstrap-4') }}
            </div>
        </div>
    </div>
@endsection
