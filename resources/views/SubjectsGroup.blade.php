@extends('layouts.dashboard')

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ asset('js/Home/home.js') }}"></script>
    <script src="{{ asset('js/Library/filterLibrary.js') }}"></script>
    <script src="{{ asset('js/materials/studentMaterials.js') }}"></script>
    @include('popper::assets')
@endsection

@section('home')

    <div id="studentMaterial" class="library">
        <div class="titleViews">Materias del grupo</div>
        <div id="buttons">
            <form class="search">
                <input value="{{ $filter }}" name="find" id='searchGroup' type="text" class="form-control"
                    placeholder="Escribe el nombre de la clase...">
                <button id="sendGroup" type="submit" class="btn"><i class="fas fa-search"></i></button>
                @if ($showClear)
                    <button id="clearGroup" type="button" class="btn btnSearch"><i class="fas fa-trash-alt"></i></button>
                @endif
            </form>

            <div id="addGroups">
                <div class="modal fade" id="addGroup" tabindex="-1" aria-labelledby="exampleAddGroups" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-body">
                                <h5>Crear grupo</h5>
                                <form method="POST" action="groups" class="text-center">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <p>Nombre del grupo</p>
                                        <input name='nameGroup' id="nameGroup" type="text" class="form-control"
                                            placeholder="Escribe el nombre del grupo..." onkeyup="return nameGroups(this);"
                                            required>
                                    </div>

                                    <div class="form-group">
                                        <p>Programa</p>
                                        <select name='selectProgram' id="selectProgram" class="form-control mb-4"
                                            onchange="selectPrograms(this.value);" required>
                                            <option value="0">Elige algún programa...</option>
                                            <option value="1">Licenciatura en derecho</option>
                                            <option value="2">Maestría en derecho</option>
                                            <option value="3">Doctorado en derecho</option>
                                            <option value="4">Jornada de titulacion licenciatura</option>
                                            <option value="5">Jornada de titulacion doctorado</option>
                                            <option value="6">Jornada de titulacion maestria</option>
                                        </select>
                                    </div>
                                    <div id="loadingSpinner" class="spinner-border mt-3" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>

                                    <div id="quarter" class="form-group">
                                        <p id="quarterP">Cuatrimestre</p>
                                        <select name="selectSemester" id="selectQuarter" class="form-control mb-4"
                                            onchange="selectQuarters(this.value);" required>
                                            <option value="">Selecciona algún Cuatrimestre...</option>
                                        </select>
                                    </div>

                                    <div id="loadingSpinner2" class="spinner-border mt-3" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>

                                    <div id="matter" class="form-group">
                                        <p>Materia</p>
                                        <select name="selectMatter[]" id="selectMatter" multiple class="form-control"
                                            onchange="selectMatters(this.value);" required>
                                            <option value="1">Materia 1</option>
                                            <option value="2">Materia 2</option>
                                            <option value="3">Materia 3</option>
                                            <option value="4">Materia 4</option>
                                            <option value="5">Materia 5</option>
                                            <option value="6">Materia 6</option>
                                        </select>
                                    </div>

                                    <div class="mt-5 mb-3">
                                        <button id="cancelGroup" type="button" class="btn"
                                            data-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn">Crear</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="class">
            <div class="messageResult">
                <h5>No se encontraron materias...</h5>
                <i class="far fa-sad-tear"></i>
            </div>
            @foreach ($listSubject as $listGroup)
                <div class="card">
                    <div class="informationSubject">
                        <div class="contentInformation">
                            <h5 class="mb-0" {{ Popper::arrow()->pop($listGroup->name) }}>{{ $listGroup->name }}</h5>
                            <p class="mb-0" {{ Popper::arrow()->pop($listGroup->nameProgram) }}>
                                {{ $listGroup->nameProgram }}</p>
                        </div>
                        <div id="optionGroups">
                            <div class="btn-group">
                                <button type="button" class="btn" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                    <span><i class="fas fa-ellipsis-v"></i></span>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item viewGroup" href="classRoom/subject/{{ $listGroup->id }}">Ver
                                        Detalles</a>
                                    <a class="dropdown-item deleteGroup" href="#"
                                        onclick="return deleteSubjectGroup({{ $groupId }},{{ $listGroup->id }})">
                                        Eliminar
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="space">
                        @php
                            $loopTeacher = 'No hay profesor';
                            $loopStudent = 0;
                        @endphp

                        @if (count($teacherNames) > 0)
                            @php($loopTeacher = $teacherNames[$loop->index]->username)
                        @endif
                        @if (count($teacherNames) > 0)
                            @php($loopStudent = $studentList[$loop->index])
                        @endif
                        <p class="ml-2">Nombre del profesor: {{ $loopTeacher }}</p>
                        <p class="ml-2 mt-2 mb-2">Alumnos inscritos:{{ $loopStudent }} </p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
