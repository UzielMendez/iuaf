@extends('layouts.dashboard')

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ asset('js/Home/home.js') }}"></script>
    <script src="{{ asset('js/materials/material.js') }}"></script>
    @include('popper::assets')
@endsection

@section('home')

    <div id="material">
        <div class="navMaterial">
            <ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="sectionMaterial-tab" data-toggle="tab" href="#sectionMaterial" role="tab"
                        aria-controls="sectionMaterial" aria-selected="true">Materia</a>
                </li>
                <li class="nav-item">
                    <a onclick="" class="nav-link" id="sectionAlumns-tab" data-toggle="tab" href="#sectionAlumns" role="tab"
                        aria-controls="alumns" aria-selected="false">Personas en la materia</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="sectionScore-tab" data-toggle="tab" href="#sectionScore" role="tab"
                        aria-controls="score" aria-selected="false">Calificaciones</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="sectionMaterial" role="tabpanel" aria-labelledby="home-tab">
                    <div id="secMaterial">
                        <h5>{{ $listGroups->nameSubject }}</h5>
                        <p>{{ $listGroups->nameProgram }}</p>
                    </div>
                    <div class="scrollSection">
                        <div>
                            <div id="teachers">
                                <h5>Profesores</h5>
                                <a id="mAddTeacher" href="#" data-toggle="modal" data-target="#modalAddTeacher"><i
                                        class="fas fa-user-plus"></i></a>
                            </div>
                            <hr>
                            <div id="listTeacher">
                                <ul id="itemTeacher">
                                    @if (count($filteredTeacher) > 0)
                                        @foreach ($filteredTeacher as $item)
                                            <li class="countTeacher">{{ $item->username }}</li>
                                        @endforeach
                                    @else
                                        <span style="justify-content: center;display:flex;">No hay personas registradas
                                            aún</span>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div>
                            <div class="modal fade" id="modalAddTeacher" tabindex="-1" role="dialog"
                                aria-labelledby="modalAddTeacherLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered justify-content-center" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <h5>Agregar Profesor</h5>
                                            <form action="">
                                                <div class="form-group text-left">
                                                    <label for="nameTeacher">Nombre del profesor</label>
                                                    <input id="nameTeacher" type="search"
                                                        placeholder="Escribe el nombre del profesor..." class="form-control"
                                                        onkeyup="search({{ $groupId }},3);">
                                                </div>
                                                <div class="spinner-border mt-3 loadingSearchTeacher" role="status">
                                                    <span class="sr-only">Loading...</span>
                                                </div>
                                                <div id="containerListTeachers">
                                                    <table style="height:0%" id="tableListTeachers">
                                                        <tbody id="listProfessor">
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="btns">
                                                    <button onclick="clearModal();" class="btn"
                                                        data-dismiss="modal">Cancelar</button>
                                                    <button
                                                        onclick="return agreePeopleInClass({{ $groupId }},{{ $subjectId }},'{{ $listGroups->nameProgram }}',{{ $listGroups->semesterNumber }},'Profesor')"
                                                        id="btnAddTeacher" type="button" class="btn"
                                                        disabled>Agregar</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div id="alumns">
                                <h5>Alumnos</h5>
                                <a id="mAddAlumns" href="#" data-toggle="modal" data-target="#modalAddAlumns"><i
                                        class="fas fa-user-plus"></i></a>
                            </div>
                            <hr>
                            <div class="listAlumns">
                                <ul id="itemStudent">
                                    @if (count($filteredStudent) > 0)
                                        @foreach ($filteredStudent as $item)
                                            <li>{{ $item->username }}</li>
                                        @endforeach
                                    @else
                                        <span style="justify-content: center;display:flex;">No hay personas registradas
                                            aún</span>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div>
                            <div class="modal fade" id="modalAddAlumns" tabindex="-1" role="dialog"
                                aria-labelledby="modalAddAlumnsLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered justify-content-center" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <h5>Agregar Alumno</h5>
                                            <form action="">
                                                <div class="form-group text-left">
                                                    <label for="nameAlumn">Nombre del alumno</label>
                                                    <input id="nameAlumn" type="search"
                                                        placeholder="Escribe el nombre del alumno..." class="form-control"
                                                        onkeyup="search({{ $groupId }},2);">
                                                </div>
                                                <div class="spinner-border mt-3 loadingSearchTeacher" role="status">
                                                    <span class="sr-only">Loading...</span>
                                                </div>
                                                <div id="containerListAlumns">
                                                    <table style="height:0%" id="tableListAlumns">
                                                        <tbody id="listStudent">
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="btns">
                                                    <button onclick="clearModal();" class="btn"
                                                        data-dismiss="modal">Cancelar</button>
                                                    <button
                                                        onclick="return agreePeopleInClass({{ $groupId }},{{ $subjectId }},'{{ $listGroups->nameProgram }}',{{ $listGroups->semesterNumber }},'Alumno')"
                                                        id="btnAddAlumn" type="button" class="btn" disabled>Agregar</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(Auth::user()->role == '1' || Auth::user()->role == '3')
                <div class="tab-pane fade" id="sectionAlumns" role="tabpanel" aria-labelledby="profile-tab">
                    <div id="searchAlumn">
                        <form id="formSearchAlumn" action="">
                            <input value="{{ $filterPeopleInClass }}" id="searchNameAlumn" name='findPeople' class="form-control" type="text"
                                placeholder="Escribe el nombre del alumno...">
                            <button type="submit" class="btn mr-2"><i class="fas fa-search"></i></button>
                            @if ($clearPeople)
                            <button id="clearAlumn" class="btn" type="button"><i class="fas fa-trash-alt m-0"></i></button>
                            @endif
                        </form>
                    </div>
                    <div class="tableListAlumn">
                        <table class="table table-hovered table-bordered">
                            <thead>
                                <tr>
                                    <td>ID</td>
                                    <td>Nombre</td>
                                    <td>Cuatrimestre</td>
                                    <td>Programa</td>
                                    <td>Categoría</td>
                                    @if(Auth::user()->role == '1')
                                    <td>Acción</td>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($peopleInClass as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->username }}</td>
                                        <td>{{ $item->semester }}</td>
                                        <td>{{ $listGroups->nameProgram }}</td>
                                        <td>{{ $item->type }}</td>
                                        @if(Auth::user()->role == '1')
                                        <td>
                                            <a id="deleteAlumns" href="#"
                                                onclick="return deleteAlumn({{ $item->id }}, {{ $groupId }}, {{ $subjectId }},'{{ $item->type }}');"><i
                                                    class="fas fa-trash-alt"></i></a>
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @endif

                @if(Auth::user()->role == '1' || Auth::user()->role == '3')
                <div class="tab-pane fade" id="sectionScore" role="tabpanel" aria-labelledby="contact-tab">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div id="searchScore">
                            <form id="formSearchScore" action="">
                                <input value="{{ $filterQualification }}" id="searchScoreAlumn" name="findQualification" class="form-control" type="text"
                                    placeholder="Escribe el nombre del alumno...">
                                <button type="submit" class="btn mr-2"><i class="fas fa-search"></i></button>
                                @if ($clearQualification)
                                <button id="clearScore" class="btn" type="button"><i
                                        class="fas fa-trash-alt m-0"></i></button>
                                @endif
                            </form>
                        </div>
                        <div id="downloadGrades">
                            <button onclick="return downloadExcelQualification({{ $groupId }},{{ $subjectId }});" type="button" class="btn">
                                <i class="far fa-file-excel"></i>
                                <span>Descargar Calificaciones</span>
                            </button>
                        </div>
                    </div>
                    <div id="tableListGrades" class="tableListAlumn">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <td>ID</td>
                                    <td>Nombre Alumno</td>
                                    <td>Asistencias</td>
                                    <td>Promedio</td>
                                    <td>Acciones</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($getQualificationStudent as $qualification)
                                    <tr>
                                        <td>{{ $qualification->id }}</td>
                                        <td>{{ $qualification->name }}</td>
                                        <td>{{ $qualification->assists }}</td>
                                        <td>{{ $qualification->average }}</td>
                                        <td>
                                            <a id="openModalGrades" href="#"
                                                onclick="return getQualitification({{ $qualification->id }});"
                                                data-toggle="modal" data-target="#modalEditGrades"><i
                                                    class="fas fa-pencil-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div>
                            <div class="modal fade" id="modalEditGrades" tabindex="-1" role="dialog"
                                aria-labelledby="modalEditGradeLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered justify-content-center" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <h5>Editar calificaciones</h5>
                                            <form class="needs-validation" action="">
                                                <div class="form-group">
                                                    <input style="display:none" name="idQualification" id="idQualification"
                                                        class="form-control" />
                                                    <label for="assists">Asistencias</label>
                                                    <input min="0" max="31" type="number" id="assists" name="assists"
                                                        type="text" placeholder="Escribe la asistencia..."
                                                        class="form-control validnumbers">
                                                </div>
                                                <div class="form-group">
                                                    <label for="average">Promedio</label>
                                                    <input id="average" name="average" type="text"
                                                        placeholder="Escribe el promedio final"
                                                        class="form-control validnumbers">
                                                </div>
                                                <div class="btns">
                                                    <button class="btn" data-dismiss="modal">Cancelar</button>
                                                    <button onclick="return updateQualification();" id="btnEditGrades"
                                                        type="button" class="btn">Agregar</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

@endsection
