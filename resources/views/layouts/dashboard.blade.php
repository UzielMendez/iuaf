<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('images/LOGO-IUAF.png') }}">
    <title>IUAF</title>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link rel="stylesheet" href="../sweetAlert/sweetalert2.css">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    @yield('scripts')
</head>

<body class="background overflow-hidden h-100" style="height:100%!important;">
    <div id="container app" class=" h-100 w-100">
        <header>
            <nav class="navbar navbar-expand">
                <a href="#" id="sandwich"><i class="fas fa-bars ml-4"></i></a>
                <div class="logo">
                    <a href="{{ asset('/home') }}">
                        <img src="{{ asset('images/LOGO-IUAF.png') }}" alt="Proximamente logo IUAF">
                        INSTITUTO UNIVERSITARIO DE ALTA FORMACIÓN
                    </a>

                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto align-items-center">
                        <i class="fas fa-user mr-0"></i>
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->username }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                 document.getElementById('logout-form').submit();">
                                        {{ __('Cerrar sesión') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </nav>
        </header>
        <main class="content">
            <aside id="MenuSandwich" class="aside">
                <ul class="list">
                    <li id="generalData">
                        <div class="category">
                            <i class="fas fa-folder"></i>
                            <a href="#">Datos generales</a>
                        </div>
                    </li>
                    <li id="schedule">
                        <div class="category">
                            <i class="fas fa-clock"></i>
                            <a href="#">Horario</a>
                        </div>
                    </li>
                    @if (Auth::user()->role == '1' || Auth::user()->role == '3')
                        <li id="qualification">
                            <div class="category">
                                <i class="fas fa-clipboard-list"></i>
                                <a href="{{ asset('/qualifications') }}">Calificaciones</a>
                            </div>
                        </li>
                    @endif
                    <li id="anthology">
                        <div class="category">
                            <i class="fas fa-book-open"></i>
                            <a href="{{ asset('/anthology') }}">Antologías</a>
                        </div>
                    </li>
                    <li id="exams">
                        <div class="category">
                            <i class="fas fa-file-alt"></i>
                            <a href="#">Exámenes</a>
                        </div>
                    </li>
                    @if (Auth::user()->role == '1')
                        @php($typeView = 'groups')
                    @else
                        @php($typeView = '')
                    @endif
                    <li id="studentMaterialPage">
                        <div class="category">
                            <i class="fas fa-book"></i>
                            <a href={{ $typeView }}>Material alumno</a>
                        </div>
                    </li>
                    @if (Auth::user()->role == '1')
                        <li id="bankSubjectsPage">
                            <div class="category">
                                <i class="fas fa-university"></i>
                                <a href="{{ asset('/bankMatters') }}">Banco Materias</a>
                            </div>
                        </li>
                    @endif
                    <li id="accountStatusPage">
                        <div class="category">
                            <i class="fas fa-wallet"></i>
                            <a href="{{ asset('/accounts') }}">Estados de cuenta</a>
                        </div>
                    </li>
                    <li id="libraryPage">
                        <div class="category">
                            <i class="fas fa-book-reader"></i>
                            <a href="{{ asset('/library') }}">Biblioteca</a>
                        </div>
                    </li>
                    @if (Auth::user()->role == '1')
                        <li id="registerPage">
                            <div class="category">
                                <i class="fas fa-users"></i>
                                <a href="{{ asset('/register') }}">Creación de Usuarios</a>
                            </div>
                        </li>
                    @endif
                </ul>
            </aside>
            @yield('home')
        </main>
    </div>
    <script src="sweetAlert/sweetalert.min.js"></script>
    @include('sweet::alert')
</body>

</html>
