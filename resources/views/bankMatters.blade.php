@extends('layouts.dashboard')

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ asset('js/Home/home.js') }}"></script>
    <script src="{{ asset('js/Library/filterLibrary.js') }}"></script>
    <script src="{{ asset('js/materials/bankSubject.js') }}"></script>
    @include('popper::assets')
@endsection

@section('home')
    <div id="bankSubjects" class="library">
        <div id="containerSubjects" class="mt-4">
            <div id="searchButtons">
                <div id="searchSubjects">
                    <form class="formSearchSubject">
                        <input id="searchSubject" value="{{ $filter }}" name="find" class="form-control" type="text"
                            placeholder="Buscar...">
                        <button class="btn btnSearch py-0 pr-0" type="submit"><i class="fas fa-search"></i></button>
                        @if ($showClear)
                            <button id="clearSubject" type="button" class="btn btnSearch pl-0 py-0"><i
                                    class="fas fa-trash-alt"></i></button>
                        @endif
                    </form>
                </div>
                <div id="buttons">
                    <button class="btn buttons" type="button" data-toggle="modal" data-target="#addMatter">Agregar
                        materia</button>
                </div>
            </div>
            <div class="addMatters">
                <div id="addMatter" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1"
                    aria-hidden="true" aria-labelledby="exampleAddMatter">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-body">
                                <h5 class="text-center">Agregar Materia</h5>
                                <form method="POST" action="bankMatters">
                                    {{ csrf_field() }}
                                    <div id="nameSubjects" class="form-group mt-4">
                                        <label for="nameSubject" class="form-label">Nombre de la materia</label>
                                        <input name="nameSubject" id="nameSubject" class="form-control" type="text"
                                            placeholder="Escribe el nombre de la materia" onkeyup="subject(this);" required>
                                    </div>
                                    <div id="programSelec" class="form-group mt-3">
                                        <label class="form-label">Programa al que pertenece</label>
                                        <select name="program" name='program' id="program" class="form-control"
                                            onchange="programs(this.value);" required>
                                            <option value="0">Elige algún programa...</option>
                                            @foreach ($programsEdit as $program)
                                                <option value="{{ $program->id }}">{{ $program->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div id="semesterSelect" class="form-group mt-3">
                                        <label class="form-label">Cuatrimestre al que pertenece</label>
                                        <select name='semester' id="semester" class="form-control"
                                            onchange="selectSemester(this.value);" required>
                                            <option value="0">Elige un programa primero...</option>
                                        </select>
                                    </div>

                                    <div class="text-center mt-4 mb-4">
                                        <button type="button" class="btn buttons cancelSubject"
                                            data-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn buttons">Guardar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="subjects" class="mt-4">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <td>Id</td>
                            <td>Nombre de la materia</td>
                            <td>Cuatrimestre</td>
                            <td>Programa</td>
                            <td>Acciones</td>
                        </tr>
                    </thead>


                    <tbody>
                        @foreach ($listSubjects as $list)
                            <tr>
                                <td>{{ $list->id }}</td>
                                <td>{{ $list->name }}</td>
                                <td>{{ $list->semesterNumber }}</td>
                                <td>{{ $list->nameProgram }}</td>
                                <td class="d-flex justify-content-center">
                                    <a id="openEdit" onclick="loaderEdit({{ $list->id }});" data-toggle="modal"
                                        data-target="#editSubject" {{ Popper::arrow()->pop('Editar') }}><i
                                            class="fas fa-pencil-alt m-0"></i></a>
                                    <a onclick="return deleteSubject({{ $list->id }})" href="#"
                                        {{ Popper::arrow()->pop('Eliminar') }}><i class="far fa-trash-alt m-0"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div id="editSubjects" class="addMatters">
                <div id="editSubject" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1"
                    aria-hidden="true" aria-labelledby="exampleEditSubject">
                    <div class="modal-dialog modal-dialog-centered">
                        <div id="modal-content" class="modal-content">
                            <div id="loadingEdit" class="spinner-border mt-3" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                            <div id="modal-body" class="modal-body">
                                <h5 class="text-center mt-3">Editar materia</h5>
                                <form class="needs-validation">
                                    <div id="editNameSubjects" class="form-group mt-4">
                                        <label for="editNameSubject" class="form-label">Nombre de la materia</label>
                                        <input style="display:none" name="idSubject" id="idSubject" class="form-control"
                                            type="text" />
                                        <input name="editNameSubject" id="editNameSubject" class="form-control" type="text"
                                            placeholder="Escribe el nombre de la materia" onkeyup="subject(this);" required>
                                    </div>
                                    <div id="editProgramSelec" class="form-group mt-3">
                                        <label class="form-label">Programa al que pertenece</label>
                                        <select name='editProgram' id="editProgram" class="form-control"
                                            onchange="programs(this.value);" required>
                                            <option value="0">Selecciona el programa..</option>
                                        </select>
                                    </div>

                                    <div id="editSemesterSelect" class="form-group mt-3">
                                        <label class="form-label">Cuatrimestre al que pertenece</label>
                                        <select name='editSemester' id="editSemester" class="form-control"
                                            onchange="selectSemester(this.value);" required>
                                            <option value="0">Selecciona el semestre..</option>
                                        </select>
                                    </div>

                                    <div class="text-center mt-4 mb-4">
                                        <button type="button" class="btn buttons cancelSubject"
                                            data-dismiss="modal">Cancelar</button>
                                        <button onclick="return updateSubject();" class="btn buttons">Actualizar</button>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page">
                {{ $listSubjects->appends(['find' => $filter])->links('pagination::bootstrap-4') }}
            </div>
        </div>
    </div>
@endsection
