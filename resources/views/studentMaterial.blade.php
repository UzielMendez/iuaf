@extends('layouts.dashboard')

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ asset('js/Home/home.js') }}"></script>
    <script src="{{ asset('js/Library/filterLibrary.js') }}"></script>
    <script src="{{ asset('js/materials/studentMaterials.js') }}"></script>
    <script src="{{ asset('js/materials/studentSubjectMaterial.js') }}"></script>
    <script src="{{ asset('js/Helpers/functions.js') }}"></script>
    @include('popper::assets')
@endsection

@section('home')

    <div id="studentMaterial" class="library">
        <div class="titleViews">Grupos</div>
        <div id="buttons">
            <form class="search">
                <input value="{{ $filter }}" name="find" id='searchGroup' type="search" class="form-control"
                    placeholder="Escribe el nombre de la clase...">
                <button id="sendGroup" type="submit" class="btn"><i class="fas fa-search"></i></button>
                @if ($showClear)
                    <button id="clearGroup" type="button" class="btn btnSearch"><i class="fas fa-trash-alt"></i></button>
                @endif
            </form>

            @if(Auth::user()->role == '1')
            <button id="btnAdd" type="button" class="btn" data-backdrop="static" data-toggle="modal" data-target="#addGroup">
                <i class="fas fa-plus mr-2"></i>
                <a href="#">Crear Grupo</a>
            </button>
            @endif
            @if(Auth::user()->role == '1')
            <div id="addGroups">
                <div class="modal fade" id="addGroup" tabindex="-1" aria-labelledby="exampleAddGroups" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-body">
                                <h5>Crear grupo</h5>
                                <form method="POST" action="groups" class="text-center">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <p>Nombre del grupo</p>
                                        <input name='nameGroup' id="nameGroup" type="text" class="form-control"
                                            placeholder="Escribe el nombre del grupo..." onkeyup="return nameGroups(this);"
                                            required>
                                    </div>
                                    <div class="form-group">
                                        <p>Programa</p>
                                        <select name='selectProgram' id="selectProgram" class="form-control mb-4"
                                            onchange="selectPrograms(this.value);" required>
                                            <option value="0">Elige algún programa...</option>
                                            @foreach ($programsEdit as $program)
                                                <option value="{{ $program->id }}">{{ $program->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div id="loadingSpinner" class="spinner-border mt-3" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>

                                    <div id="quarter" class="form-group">
                                        <p id="quarterP">Cuatrimestre</p>
                                        <select name="selectSemester" id="selectQuarter" class="form-control mb-4"
                                            onchange="selectQuartersModalAdd();" required>
                                            <option value="">Selecciona algún Cuatrimestre...</option>
                                        </select>
                                    </div>

                                    <div id="loadingSpinner2" class="spinner-border mt-3" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>

                                    <div id="matter" class="form-group">
                                        <p>Materia</p>
                                        <select name="selectMatter[]" id="selectMatter" multiple class="form-control"
                                            onchange="selectMatters(this.value);" required>
                                        </select>
                                    </div>

                                    <div class="mt-5 mb-3">
                                        <button id="cancelGroup" type="button" class="btn"
                                            data-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn">Crear</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>

        <div id="class">
            <div class="messageResult">
                <h5>No se encontraron grupos...</h5>
            </div>
            @foreach ($listGroups as $listGroup)
                <div class="card">
                    <div class="information">
                        <div class="contentInformation">
                            <h5 class="mb-0" {{ Popper::arrow()->pop($listGroup->name) }}>{{ $listGroup->name }}</h5>
                            <p class="mb-0" {{ Popper::arrow()->pop($listGroup->nameProgram) }}>{{ $listGroup->nameProgram }}</p>
                        </div>
                        <div id="optionGroups">
                            <div class="btn-group">
                                <button type="button" class="btn" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                    <span><i class="fas fa-ellipsis-v"></i></span>
                                </button>
                                <div class="dropdown-menu">                                    
                                    <button style="width:96%; padding-left:17px;" class="btn dropdown-item viewGroup" onclick="location.href='{{url('subjectsGroup/'.$listGroup->id.'/subject')}}'" >Ver</button>
                                    <a id="buttonEditGroup" onclick="getSubjectGroup({{ $listGroup->id }});" type="button" class="dropdown-item"
                                        data-toggle="modal" data-target="#editGroups" class="dropdown-item editGroup"
                                        href="#">Editar</a>
                                    <a onclick="return deleteGroup({{ $listGroup->id }})"
                                        class="dropdown-item deleteGroup" href="#">Eliminar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="space">
                        <p class="ml-2 mt-2 mb-2">Total de materias en el grupo: {{$subjectList[$loop->index]}}</p>
                    </div>
                </div>
            @endforeach
        </div>

        <div id="editGroup">
            <div class="modal fade" id="editGroups" tabindex="-1" data-backdrop="static"   aria-labelledby="exampleEditGroup" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div id="modal-contentGroup" class="modal-content">
                        <div id="loadingEditGroup" class="spinner-border mt-3" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div style="padding:0px" id="modal-bodyGroup" class="modal-body">
                            <h5 class="text-center text-bold">Editar Grupo</h5>
                            <form id="modalEditGroups" class="text-center ml-4 mr-4 ">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <p>Nombre del grupo</p>
                                    <div class="d-flex align-items-center">
                                        <input style="display:none" name="idGroup" id="idGroup" class="form-control"/>
                                        <input name='nameGroup' id="editNameGroup" type="text" class="form-control"
                                            placeholder="Escribe el nombre del grupo..." required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p>Programa</p>
                                    <select name='selectProgram' id="editSelectProgram" class="form-control mb-4"
                                        onchange="selectPrograms(this.value);" required>
                                        <option value="0">Elige algún programa...</option>
                                    </select>
                                </div>

                                <div id="quarter" class="form-group">
                                    <p id="quarterP">Cuatrimestre</p>
                                    <select name="selectSemester" id="editSelectQuarter" class="form-control mb-4"
                                        onchange="selectQuarters(this.value);" required>
                                        <option value="0">Selecciona algún Cuatrimestre...</option>
                                    </select>
                                </div>

                                <div>
                                    <p>Materias del grupo</p>
                                    <div id="subjectsTable" class="books form-group">
                                        <table class="subjectsTable table table-hover table-sm">
                                            <tbody id="matters">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div id="checkAddNewSubjects">
                                    <label for="checkSubject"><input id="checkSubject" type="checkbox">Agregar nuevas
                                        materias</label>
                                </div>
                                <div id="newSubjects">
                                    <div id="addNewSubjects" class="mt-3">
                                        <input id="findsubjects" class="form-control" type="search" placeholder="Materia a buscar...">
                                        <button type="button" onclick="return findSubject();" id="searchNewSubjects" class="btn"><i class="fas fa-search"></i></button>
                                    </div>

                                    <div id="loadingNewSubject" class="spinner-border mt-3" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>

                                    <div id="emptyMessage" class="d-none">
                                        <h5 class="mb-0">Lo siento no hay resultados...</h5>
                                        <i class="far fa-sad-cry"></i>
                                    </div>

                                    <div id="tableNewSubjects" class="books form-group mt-3">
                                        <table class="tableNewSubjects table table-hover table-sm">
                                            <thead>
                                                <tr>
                                                    <td>Nombre de la materia</td>
                                                </tr>
                                            </thead>
                                            <tbody id="listAllMatters">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="buttons mt-5 mb-3">
                                    <button id="cancelEditGroup" type="button" class="btn"
                                        data-dismiss="modal">Cancelar</button>
                                    <button type="button" onclick="return updateGroup();" class="btn">Actualizar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
