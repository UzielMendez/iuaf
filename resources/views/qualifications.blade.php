@extends('layouts.dashboard')

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ asset('js/Home/home.js') }}"></script>
    <script src="{{ asset('js/qualifications/qualifications.js') }}"></script>
    @include('popper::assets')
@endsection

@section('home')
    <div id="gradesModules">
        <div id="searchGradesContainer" class="d-flex justify-content-between align-items-center flex-wrap">
            <div class="searchQualification">
                <form id="formSearchQualification" method="GET">
                    <select name="optionsQualification" id="optionsQualification" class="form-control mb-3 mr-3"
                        onchange="selectFilter(this.value);">
                        <option value="">Filtrar por...</option>
                        <option value="name">nombre</option>
                        <option value="program">programa</option>
                        <option value="subject">materia</option>
                    </select>
                    <div id="containerOptions">
                        <input value="{{ $searchAlumn }}" id="searchNameAlumn" class="form-control" name="findName"
                            type="text" placeholder="Buscar por nombre...">

                        <input value="{{ $searchProgram }}" id="searchProgramAlumn" class="form-control"
                            name="findProgram" type="text" placeholder="Buscar por programa...">

                        <input value="{{ $searchSubject }}" id="searchSubjectAlumn" class="form-control"
                            name="findSubject" type="text" placeholder="Buscar por materia...">

                        <button id="btnSearchQualification" type="submit" class="btn mr-2"><i
                                class="fas fa-search"></i></button>
                        @if ($showClear)
                            <button onclick="btnReset();" id="clearScore" class="btn" type="submit"><i
                                    class="fas fa-trash-alt m-0"></i></button>
                        @endif
                    </div>
                </form>
            </div>
            <div id="downloadGradesGlobals">
                <button onclick="return downloadExcelQualifications();" type="button" class="btn">
                    <i class="far fa-file-excel"></i>
                    <span>Descargar Calificaciones</span>
                </button>
            </div>
        </div>
        <div style="font-size: 30px;width: 100%;display: flex;justify-content: center;font-family: sans-serif;"
            class="form-group">
            <h5 style="font-size: 30px">Calificaciones</h5>
        </div>
        <div id="gradesGlobals">
            <table class="table table-hover table-bordered text-center">
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Nombre</td>
                        <td>Promedio</td>
                        <td>Cuatrimestre</td>
                        <td>Programa</td>
                        <td>Materia</td>
                        <td>Grupo</td>
                        {{-- <td>Acciones</td> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach ($listAlumns as $listAlumn)
                        <tr>
                            <td>{{ $listAlumn->id }}</td>
                            <td>{{ $listAlumn->name }}</td>
                            <td>{{ $listAlumn->average }}</td>
                            <td>{{ $listAlumn->semesterNumber }}</td>
                            <td>{{ $listAlumn->nameProgram }}</td>
                            <td>{{ $listAlumn->nameSubject }}</td>
                            <td>{{ $listAlumn->nameGroup }}</td>
                            {{-- <td>
                                <a href="#" data-toggle="modal" data-target="#editQualification"><i
                                        class="fas fa-pencil-alt"></i></a>
                            </td> --}}
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{-- <div>
                <div class="modal fade" id="editQualification" tabindex="-1" role="dialog"
                    aria-labelledby="editQualificationLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered justify-content-center" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <h5>Editar calificaciones</h5>
                                <form class="needs-validation" novalidate>
                                    <div class="form-group">
                                        <label for="averageQ">Promedio</label>
                                        <input min="0" max="10" id="averageQ" name="averageQ" type="number"
                                            placeholder="Escribe el promedio final" class="form-control validnumbers"
                                            required>
                                        <div class="invalid-feedback validations">
                                            Porfavor escribe el promedio.
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="semesterQ">Cuatrimestre</label>
                                        <input min="0" max="31" type="number" id="semesterQ" name="semesterQ"
                                            placeholder="Escribe la asistencia..." class="form-control validnumbers"
                                            required>
                                        <div class="invalid-feedback validations">
                                            Porfavor escribe el cuatrimestre                                        </div>
                                    </div>
                                    <div class="btns">
                                        <button onclick="clearModal();" class="btn" data-dismiss="modal">Cancelar</button>
                                        <button id="btnEditGrades" type="submit" class="btn">Editar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>
@endsection
