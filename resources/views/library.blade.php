@extends('layouts.dashboard')

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ asset('js/Home/home.js') }}"></script>
    <script src="{{ asset('js/Library/filterLibrary.js') }}"></script>
    @include('popper::assets')
@endsection

@section('home')
    <div class=" library h-100 w-100 bg-white pl-5">
        <div class="everywhere w-100 d-flex">
            <div class="logolibrary align-self-center">
                <img src="{{ asset('images/iconLibrary.png') }}" alt="Logo">
                <h5>Biblioteca Digital</h5>
            </div>
        </div>

        <div id="SearchBook" class="h-50">
            <div id="buttonsSearch">
                <form id="sendPetititon" class="form-group">
                    {{ csrf_field() }}
                    <div class="containeInputFilter w-100">
                        <input value="{{ $filter }}" id='searchLibrary' name="find" class="form-control bg-white"
                            type="search" placeholder="Escribe el titulo del libro..." aria-label="Search">
                        @if ($showClear)
                            <button id="clearButton" {{ Popper::arrow()->pop('Eliminar') }} type="button"
                                class="btn btnSearch p-0"><i class="fas fa-trash-alt"></i></button>
                        @endif
                        <button {{ Popper::arrow()->pop('Buscar') }} id='applyFilter' class="btn btnSearch"
                            type="submit"><i class="fas fa-search"></i></button>
                    </div>
                </form>
                @if (Auth::user()->role == '1')
                    <div id="addBookButton">
                        <button type="button" class="btn btnAddBook" data-toggle="modal" data-target="#addBooks">Agregar
                            libro</button>
                    </div>
                @endif
                <div id="addBookModal">
                    <div class="modal fade" id="addBooks" tabindex="-1" role="dialog" data-backdrop="static"
                        aria-labelledby="exampleAddBooks" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content modal-contentBook">
                                <div class="modal-body">
                                    <h5>Agregar libro</h5>
                                    <form action="">
                                        <div class="form-group">
                                            <label for="nameBook" class="form-label labelNameBook">Nombre libro</label>
                                            <input id="nameBook" type="text" class="form-control"
                                                placeholder="Escribe el nombre del libro...">
                                        </div>
                                        <div class="form-group">
                                            <label for="nameAuthor" class="form-label labelAuthor">Autor</label>
                                            <input id="nameAuthor" type="text" class="form-control"
                                                placeholder="Escribe el nombre del autor...">
                                        </div>

                                        <div id="selectBook" class="input-group mb-3 mt-3 flex-column">
                                            <label class="labelUploadBook">Elige tu archivo</label>
                                            <div class="custom-file w-100">
                                                <input type="file" class="custom-file-input" id="uploadBook">
                                                <label class="custom-file-label" for="uploadBook">Seleccionar
                                                    archivo...</label>
                                            </div>
                                        </div>
                                        <div class="buttonsBook">
                                            <button type="button" class="btn" data-dismiss="modal">Cancelar</button>
                                            <button type="submit" class="btn">Agregar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="books h-100">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>Nombre del libro</td>
                            <td>Autor del libro</td>
                            <td>Acciones</td>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($listBooks as $book)
                            <tr>
                                <td>{{ $book->id }}</td>
                                <td>{{ $book->bookName }}</td>
                                <td>{{ $book->bookAuthor }}</td>
                                <td id="actions">
                                    <a target="_blank" href="{{ asset('bookPdf/' . $book->bookFile) }}"
                                        {{ Popper::arrow()->pop('ver') }}><i class="fas fa-eye m-0"></i></a>
                                    @if(Auth::user()->role == '1')
                                        <a onclick="showBook({{ $book->id }});" href="#"
                                        {{ Popper::arrow()->pop('Editar') }} data-toggle="modal"
                                        data-target="#editBooks"><i class="fas fa-pencil-alt m-0"></i></a>
                                    <a onclick="return deleteBook({{ $book->id }})" href="#"
                                        {{ Popper::arrow()->pop('Eliminar') }}><i class="far fa-trash-alt m-0"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="page">
                {{ $listBooks->appends(['find' => $filter])->links('pagination::bootstrap-4') }}
            </div>
            <div id="editBookModal">
                <div class="modal fade" id="editBooks" tabindex="-1" role="dialog" data-backdrop="static"
                    aria-labelledby="exampleEditBooks" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content modal-contentBook">
                            <div class="modal-body">
                                <h5>Editar libro</h5>
                                <form action="">
                                    <div class="form-group">
                                        <input id="idBooks" name="idBooks" class="d-none" type="text">
                                        <label for="nameBooks" class="form-label labelNameBook">Nombre libro</label>
                                        <input id="nameBooks" name="nameBooks" type="text" class="form-control"
                                            placeholder="Escribe el nombre del libro...">
                                    </div>
                                    <div class="form-group">
                                        <label for="nameAuthors" class="form-label labelAuthor">Autor</label>
                                        <input id="nameAuthors" type="text" class="form-control"
                                            placeholder="Escribe el nombre del autor...">
                                    </div>

                                    <div class="input-group mb-3 mt-3 flex-column">
                                        <label class="labelUploadBooks">Elige tu archivo</label>
                                        <div class="custom-file w-100">
                                            <input type="file" class="custom-file-input" id="uploadBooks">
                                            <label class="upBook custom-file-label" for="uploadBooks">Seleccionar
                                                archivo...</label>
                                        </div>
                                    </div>
                                    <div class="buttonsBook">
                                        <button type="button" class="btn" data-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn">Actualizar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
