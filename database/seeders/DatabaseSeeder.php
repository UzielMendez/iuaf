<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Role;
use App\Models\Permission;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET foreign_key_checks=0');
        DB::table('role_user')->truncate();
        DB::table('permission_role')->truncate();    
        Permission::truncate();
        Role::truncate();
        DB::statement('SET foreign_key_checks=1');

        $this->call(RolesSeeder::class);

        $userAdmin = User::where('email','admin@gmail.com')->first();
        if($userAdmin){
            $userAdmin->delete();
        }

        $userAdmin = User::create([
            'username' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('12345678'),
        ]);

         $roleAdmin = Role::create([
            'name'=> 'Admin',
            'slug'=> 'admin',
            'description'=> 'Aministrator',
            'full-access'=> 'yes'
        ]);

        $roleUser = Role::create([
            'name'=> 'Student User',
            'slug'=> 'registeredstudent',
            'description'=> 'Student User',
            'full-access'=> 'no'
        ]);

        $userAdmin->roles()->sync([$roleAdmin->id]);

        $permissionList = [];
         $permission =  Permission::create([
              'name'=> 'Permiso para acceder a la libreria ejemplo ',
              'slug'=> 'libraryExample.index',
              'description'=> 'Este usuario puede acceder a la vista libraryExample al metodo index',
          ]);
          
          $permissionList[]= $permission->id;
          
          $permission =  Permission::create([
            'name'=> 'Permiso para crear a la libreria ejemplo ',
            'slug'=> 'libraryExample.create',
            'description'=> 'Este usuario puede agregar un libro  a la vista libraryExample al metodo crear',
        ]);

          $permissionList[]= $permission->id;

        $roleAdmin->permissions()->sync($permissionList);
    }
}
