<?php

namespace App\Exports;

use App\Models\Qualification;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromQuery;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Support\Facades\DB;

class QualificationsReport implements FromQuery
{
  /**
   * @return \Illuminate\Support\Collection
   */
  use Exportable;

  public function __construct(string $type =  null, string $typeFilter =  null)
  {
    $this->filter = $typeFilter;
    $this->type = $type;
  }

  public function query()
  {

    if (is_null($this->type)) {

      return Qualification::query()->select(
        "users.id",
        "users.username",
        "qualifications.average",
        "qualifications.semesterNumber",
        "programs.nameProgram",
        "subjects.name",
        "groups.name"
      )
        ->join('users', 'users.id', '=', 'qualifications.idUser')
        ->join('programs', 'programs.id', '=', 'qualifications.idProgram')
        ->join('subjects', 'subjects.id', '=', 'qualifications.idSubject')
        ->join('groups', 'groups.id', '=', 'qualifications.idGroup');
    } else {
      $actualFilter = null;
      if ($this->type == 'name') {
        $actualFilter = 'users.username';
      } else if ($this->type == 'program') {
        $actualFilter = 'programs.nameProgram';
      } else if ($this->type == 'subject') {
        $actualFilter = 'subjects.name';
      }
      return Qualification::query()->select(
        "users.id",
        "users.username",
        "qualifications.average",
        "qualifications.semesterNumber",
        "programs.nameProgram",
        "subjects.name",
        "groups.name"
      )
        ->join('users', 'users.id', '=', 'qualifications.idUser')
        ->join('programs', 'programs.id', '=', 'qualifications.idProgram')
        ->join('subjects', 'subjects.id', '=', 'qualifications.idSubject')
        ->join('groups', 'groups.id', '=', 'qualifications.idGroup')
        ->where($actualFilter, 'LIKE', '%' . $this->filter . '%');
    }
  }
}
