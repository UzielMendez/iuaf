<?php

namespace App\Exports;

use App\Models\Qualification;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromQuery;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Support\Facades\DB;

class QualificationReport implements FromQuery
{
  /**
   * @return \Illuminate\Support\Collection
   */
  use Exportable;

  public function __construct(int $idGroup, int $idSubject, $userToFilter = NULL)
  {
    $this->idGroup = $idGroup;
    $this->nameUserToFilter = $userToFilter;
    $this->idSubject = $idSubject;
  }

  public function query()
  {

    if (is_null($this->nameUserToFilter)) {

      return Qualification::query()->select(
        "qualifications.id",
        "users.username",
        "qualifications.assists",
        "qualifications.average"
      )
        ->join('users', 'users.id', '=', 'qualifications.idUser')
        ->where('qualifications.idGroup', '=', $this->idGroup)
        ->where('qualifications.idSubject', '=', $this->idSubject)
        ->where('qualifications.status', true);
    } else {

      return Qualification::query()->select(
        "qualifications.id",
        "users.username",
        "qualifications.assists",
        "qualifications.average"
      )
        ->join('users', 'users.id', '=', 'qualifications.idUser')
        ->where('qualifications.idGroup', '=', $this->idGroup)
        ->where('qualifications.idSubject', '=', $this->idSubject)
        ->where('qualifications.status', true)
        ->where('users.username', 'LIKE', '%' . $this->nameUserToFilter . '%');
    }
  }
}
