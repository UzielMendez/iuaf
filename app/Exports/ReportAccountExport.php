<?php

namespace App\Exports;

use App\Models\AccountStatus;
use Maatwebsite\Excel\Concerns\FromCollection;

class ReportAccountExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return AccountStatus::all();
    }
}
