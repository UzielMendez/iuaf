<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PeoplesGroup extends Model
{
    protected $table = 'peoples_group';
     public $timestamps = false;
    
}
