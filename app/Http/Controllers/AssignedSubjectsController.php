<?php

namespace App\Http\Controllers;

use App\Models\AssignedSubjects;
use Illuminate\Http\Request;

class AssignedSubjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AssignedSubjects  $assignedSubjects
     * @return \Illuminate\Http\Response
     */
    public function show(AssignedSubjects $assignedSubjects)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AssignedSubjects  $assignedSubjects
     * @return \Illuminate\Http\Response
     */
    public function edit(AssignedSubjects $assignedSubjects)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AssignedSubjects  $assignedSubjects
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssignedSubjects $assignedSubjects)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AssignedSubjects  $assignedSubjects
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssignedSubjects $assignedSubjects)
    {
        //
    }
}
