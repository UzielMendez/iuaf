<?php

namespace App\Http\Controllers;

use App\Models\StudentCategories;
use Illuminate\Http\Request;

class StudentCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StudentCategories  $studentCategories
     * @return \Illuminate\Http\Response
     */
    public function show(StudentCategories $studentCategories)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StudentCategories  $studentCategories
     * @return \Illuminate\Http\Response
     */
    public function edit(StudentCategories $studentCategories)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StudentCategories  $studentCategories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudentCategories $studentCategories)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StudentCategories  $studentCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy(StudentCategories $studentCategories)
    {
        //
    }
}
