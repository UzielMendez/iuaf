<?php

namespace App\Http\Controllers;

use App\Models\Subjects;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Programs;

class SubjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->get('find');
        $showClear = false;
         $listSubjects = DB::table('subjects')
         ->join('programs', 'programs.id', '=', 'subjects.idProgram')
         ->select('subjects.id','subjects.name', 'subjects.semesterNumber','programs.nameProgram as nameProgram')
         ->where('subjects.status' ,'=', 1)
         ->paginate(10);
         $programsEdit = Programs::select(
            'programs.id',
            'programs.nameProgram as name',
            'programs.semesterNumber',
            )
        ->get();

        if($filter== ''){
         return view('bankMatters', compact('listSubjects', 'filter','showClear','programsEdit'));
        }
        $getDataLibrary = DB::table('subjects')
        ->join('programs', 'programs.id', '=', 'subjects.idProgram')
        ->select('subjects.id','subjects.name', 'subjects.semesterNumber','programs.nameProgram as nameProgram')
        ->where('subjects.status' ,'=', 1)
        ->where('subjects.name' ,'LIKE', '%'.$filter.'%')
        ->orWhere('subjects.status' ,'=', 1)
        ->where('programs.nameProgram' ,'LIKE', '%'.$filter.'%')
        ->paginate(10);

       

        if(count($getDataLibrary)> 0){
            $listSubjects = $getDataLibrary;
            $showClear = true; 
         }

        return view('bankMatters', compact('listSubjects', 'filter','showClear', 'programsEdit'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestSubject = request()->except('_token');
        $checkName = $requestSubject['nameSubject'];
        $checkProgram = $requestSubject['program'];
        $isExist = DB::table('subjects')
        ->where('name', $checkName)
        ->where('idProgram',$checkProgram )
        ->first();

        if (empty($isExist)) {
            
            $newGroup  = Subjects::insert([
                'name' => $requestSubject['nameSubject'],
                'semesterNumber' => $requestSubject['semester'],
                'dateStart' =>  date('Y-m-d H:i:s'),
                'idProgram' => $requestSubject['program'],
                'status' => true
            ]);

        alert()->success('Éxito', 'Se agregó la materia correctamente correctamente');
            
        } else {
            alert()->error('Upss', 'Lo sentimos, la materia ya se registró en este programa anteriormente');
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Subjects  $subjects
     * @return \Illuminate\Http\Response
     */
    public function show(Subjects $subjects)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Subjects  $subjects
     * @return \Illuminate\Http\Response
     */
    public function edit($subjectId)
    {
        if($subjectId != null ||$subjectId != 0) {
            $subject = Subjects::select(
                'subjects.id',
                'subjects.name',
                'subjects.semesterNumber',
                'programs.nameProgram')
                ->join('programs', 'programs.id', '=', 'subjects.idProgram')
                ->where('subjects.id', '=',$subjectId)
                ->first();
            $programsEdit = Programs::select(
                'programs.id',
                'programs.nameProgram as name',
                'programs.semesterNumber',
                )
                ->get();
            $filter = '';
            $showClear = false;

           return [$subject,$programsEdit] ;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Subjects  $subjects
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $requestGroup = request()->except('_token');
        $isExist = Subjects::where('name', '=',  $requestGroup['name'])
        ->where('idProgram', '=',$requestGroup['program'])
        ->where('semesterNumber', '=',$requestGroup['semester'])
        ->first();
        if(empty($isExist)){
            $result = Subjects::where('id', '=',$id)->update(
                [
                    'name' => $requestGroup['name'],
                    'idProgram' => $requestGroup['program'],
                    'semesterNumber' => $requestGroup['semester'],
                ]
            );
             return $result;
        }else{
            return 0;
        }
    }

    public function disableGroup(Request $request)
    {
        $requestGroup = request()->except('_token');

        $result = Subjects::where('id', '=',$requestGroup['subjectId'])->update(['status' => "0"]);
        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Subjects  $subjects
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subjects $subjects)
    {
        //
    }
}
