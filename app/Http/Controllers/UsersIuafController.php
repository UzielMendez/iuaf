<?php

namespace App\Http\Controllers;

use App\Models\UsersIuaf;
use Illuminate\Http\Request;

class UsersIuafController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UsersIuaf  $usersIuaf
     * @return \Illuminate\Http\Response
     */
    public function show(UsersIuaf $usersIuaf)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UsersIuaf  $usersIuaf
     * @return \Illuminate\Http\Response
     */
    public function edit(UsersIuaf $usersIuaf)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UsersIuaf  $usersIuaf
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UsersIuaf $usersIuaf)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UsersIuaf  $usersIuaf
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsersIuaf $usersIuaf)
    {
        //
    }
}
