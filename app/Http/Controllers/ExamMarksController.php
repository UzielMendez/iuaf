<?php

namespace App\Http\Controllers;

use App\Models\ExamMarks;
use Illuminate\Http\Request;

class ExamMarksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ExamMarks  $examMarks
     * @return \Illuminate\Http\Response
     */
    public function show(ExamMarks $examMarks)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ExamMarks  $examMarks
     * @return \Illuminate\Http\Response
     */
    public function edit(ExamMarks $examMarks)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ExamMarks  $examMarks
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExamMarks $examMarks)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ExamMarks  $examMarks
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExamMarks $examMarks)
    {
        //
    }
}
