<?php

namespace App\Http\Controllers;

use App\Models\BookLibrary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookLibraryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->get('find');
        $showClear = false;
        $listBooks = BookLibrary::paginate(10);

        if($filter== ''){
         return view('library', compact('listBooks', 'filter','showClear'));
        }
        $getDataLibrary = DB::table('book_library')
        ->where('book_library.bookName' ,'LIKE', '%'.$filter.'%')
        ->orWhere('book_library.bookAuthor' ,'LIKE', '%'.$filter.'%')
        ->paginate(10);

        if(count($getDataLibrary)> 0){
            $listBooks = $getDataLibrary;
            $showClear = true; 
         }
        return view('library', compact('listBooks', 'filter','showClear'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BookLibrary  $bookLibrary
     * @return \Illuminate\Http\Response
     */
    public function show(BookLibrary $bookLibrary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BookLibrary  $bookLibrary
     * @return \Illuminate\Http\Response
     */
    public function edit($editBooks)
    {
        if($editBooks != null || $editBooks != 0){
            $editBook = BookLibrary::select(
                'book_library.id',
                'book_library.bookName',
                'book_library.bookAuthor',
                'book_library.bookFile')
                ->where('book_library.id', '=', $editBooks)
                ->first();

            return [$editBook];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BookLibrary  $bookLibrary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BookLibrary $bookLibrary)
    {
        //
    }

    public function disableBooks(Request $request)
    {
        $requestBook = request()->except('_token');

        $result = BookLibrary::where('id', '=',$requestBook['editBooks'])->update(['bookState' => "0"]);
        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BookLibrary  $bookLibrary
     * @return \Illuminate\Http\Response
     */
    public function destroy(BookLibrary $bookLibrary)
    {
        //
    }
}
