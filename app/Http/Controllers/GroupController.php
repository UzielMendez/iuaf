<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Subjects;
use App\Models\Programs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->get('find');
        $showClear = false;
        $listGroups = DB::table('groups')
            ->join('programs', 'programs.id', '=', 'groups.idProgram')
            ->select('groups.id', 'groups.name', 'groups.semesterNumber', 'programs.nameProgram as nameProgram')
            ->where('groups.status', '=', 1)
            ->paginate(10);
        $programsEdit = Programs::select(
            'programs.id',
            'programs.nameProgram as name',
            'programs.semesterNumber',
        )
            ->get();

        $subjectList =  [];
        foreach ($listGroups as $group) {
            $subjects  = DB::table('groups_courses')
                ->join('groups', 'groups_courses.idGroup', '=', "groups.id")
                ->where('groups_courses.idGroup', $group->id)
                ->where('groups_courses.status', true)
                ->count();

            array_push($subjectList, $subjects);
        }

        if ($filter == '') {
            return view('studentMaterial', compact('listGroups', 'filter', 'showClear', 'programsEdit', 'subjectList'));
        }
        //Filter
        $getDataLibrary = DB::table('groups')
            ->join('programs', 'programs.id', '=', 'groups.idProgram')
            ->select('groups.id', 'groups.name', 'groups.semesterNumber', 'programs.nameProgram as nameProgram')
            ->where('groups.status', '=', 1)
            ->where('groups.name', 'LIKE', '%' . $filter . '%')
            ->orWhere('groups.status', '=', 1)
            ->where('programs.nameProgram', 'LIKE', '%' . $filter . '%')
            ->paginate(10);

        if (count($getDataLibrary) > 0) {
            $listGroups = $getDataLibrary;
            $showClear = true;
        }
        return view('studentMaterial', compact('listGroups', 'filter', 'showClear', 'programsEdit', 'subjectList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestGroup = request()->except('_token');

        $requestAccount = request()->except('_token');
        $checkName = $requestAccount['nameGroup'];
        $isExist = DB::table('groups')->where('name', $checkName)->first();
        if (empty($isExist)) {

            $newGroup  = Group::insert([
                'name' => $requestAccount['nameGroup'],
                'semesterNumber' => $requestAccount['selectSemester'],
                'idProgram' => $requestAccount['selectProgram'],
                'status' => true
            ]);

            if ($newGroup == 1) {
                //Obtengo el ultimo registro 
                $getLastId = Group::latest('id')->first();
                for ($i = 0; $i < count($requestAccount['selectMatter']); $i++) {
                    DB::table('groups_courses')->insert(
                        [
                            'idGroup' => $getLastId->id,
                            'idSubject' => $requestAccount['selectMatter'][$i],
                            'status' => true
                        ]
                    );
                }
            }
            alert()->success('Éxito', 'Se agregó el grupo correctamente');
        } else {
            alert()->error('Upss', 'Lo sentimos, este grupo ya se registró anteriormente');
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit($groupId)
    {
        if ($groupId != null || $groupId != 0) {
            $result = Group::select(
                'groups.id',
                'groups.name AS nameGroup',
                'groups.semesterNumber',
                'programs.id as idProgram',
                'programs.nameProgram',
                'programs.semesterNumber',
                'subjects.name AS nameSubject'
            )
                ->join('programs', 'programs.id', '=', 'groups.idProgram')
                ->join('groups_courses', 'groups.id', '=', 'groups_courses.idGroup')
                ->join('subjects', 'subjects.id', '=', 'groups_courses.idSubject')
                ->where('groups.id', '=', $groupId)
                ->first();

            $getSubjectLibrary = DB::table('groups_courses')
                ->join('subjects', 'groups_courses.idSubject', '=', 'subjects.id')
                ->select('subjects.name')
                ->where('groups_courses.idGroup', '=', $groupId)
                ->where('groups_courses.status', '=', true)
                ->get();

            $list = [];
            foreach ($getSubjectLibrary as $key) {
                array_push($list, $key->name);
            }

            $getSubjectProgram = DB::table('subjects')
                ->join('programs', 'subjects.idProgram', '=', 'programs.id')
                ->select('subjects.name', 'subjects.id')
                ->where('programs.nameProgram', '=', $result->nameProgram)
                ->where('subjects.status', '=', '1')
                ->whereNotIn('subjects.name', $list)
                ->get();
            $allPrograms = Programs::select(
                'programs.id',
                'programs.nameProgram as name',
                'programs.semesterNumber',
            )
                ->get();

            return [$result, $getSubjectLibrary, $getSubjectProgram, $allPrograms];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestGroup = request()->except('_token');
        $actionSucces  = 1;

        $result = Group::where('id', '=', $id)->update(
            [
                'name' => $requestGroup['name'],
                'idProgram' => $requestGroup['program'],
                'semesterNumber' => $requestGroup['semester'],
            ]
        );
        // if ($result == 0) {
        //     $actionSucces = 0;
        // }
    
         for ($i = 0; $i < count($requestGroup['listSubjects']); $i++) {
             $findSubject = DB::table('subjects')->where('name', $requestGroup['listSubjects'][$i])->first();
             if (!empty($findSubject)) {
                 $listSbjectGroups = DB::table('groups_courses')
                     ->where('idGroup', $id)
                     ->where('idSubject', $findSubject->id)->first();

                 if (empty($listSbjectGroups)) {
                     DB::table('groups_courses')->insert(
                         [
                             'idGroup' => $id,
                             'idSubject' => $findSubject->id,
                             'status' => true
                         ]
                     );
                     $actionSucces = 0;
                 }else{
                     DB::table('groups_courses')
                     ->where('idGroup', $id)
                     ->where('idSubject', $findSubject->id)
                     ->update(
                         [
                             'status' => true
                         ]
                     );
                     $actionSucces = 1;
                 }
             }
         }

         return $actionSucces;
    }
    public function disableGroup(Request $request)
    {
        $requestGroup = request()->except('_token');

        $result = Group::where('id', '=', $requestGroup['groupId'])->update(['status' => "0"]);
        return $result;
    }
    public function disableSubject(Request $request, $idSubject)
    {
        $requestGroup = request()->except('_token');
        $result = DB::table('groups_courses')
            ->where('idGroup', '=', $requestGroup['groupId'])
            ->where("idSubject", "=", $idSubject)
            ->update(['status' => "0"]);
        return $result;
    }
    public function findSubject()
    {
        $requestGroup = request()->except('_token');
        $result = Subjects::select('name')
            ->where('idProgram', '=',  $requestGroup['idProgram'])
            ->where('status', '=',  true)
            ->where('name', 'LIKE', '%' . $requestGroup['subject'] . '%')
            ->whereNotIn('name', $requestGroup['listSubjects'])
            ->get();
        return $result;
    }

    public function findSubjectToProgram($programId)
    {
        $result = Subjects::select('id', 'name')
            ->where('idProgram', '=', $programId)
            ->where('status', '=', '1')
            ->get();
        return $result;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        //
    }
}
