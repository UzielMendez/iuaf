<?php

namespace App\Http\Controllers;

use App\Models\Programs;
use App\Models\Qualification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Exports\QualificationsReport;

class QualificationControllerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $showClear = false;
        $getValue = null;
        $options = $request->get('optionsQualification');
        $searchAlumn = $request->get('findName');
        $searchProgram = $request->get('findProgram');
        $searchSubject = $request->get('findSubject');

        $listAlumns = DB::table('qualifications')
            ->join('users', 'users.id', '=', 'qualifications.idUser')
            ->join('programs', 'programs.id', '=', 'qualifications.idProgram')
            ->join('subjects', 'subjects.id', '=', 'qualifications.idSubject')
            ->join('groups', 'groups.id', '=', 'qualifications.idGroup')
            ->select('users.id', 'users.username as name', 'qualifications.average', 'qualifications.semesterNumber as semesterNumber', 'programs.nameProgram as nameProgram', 'subjects.name as nameSubject', 'groups.name as nameGroup')
            ->get();

        $programs = Programs::select(
            'programs.id',
            'programs.nameProgram as name',
            'programs.semesterNumber',
        )
            ->get();

        if ($options == '') {
            return view('qualifications', compact('listAlumns', 'showClear', 'getValue', 'options', 'searchAlumn', 'searchProgram', 'searchSubject'));
        }
        if ($options == 'name') {
            $getValue = $searchAlumn;
        } else if ($options == 'program') {
            $getValue = $searchProgram;
        } else if ($options == 'subject') {
            $getValue = $searchSubject;
        }

        $listAlumns = DB::table('qualifications')
            ->join('users', 'users.id', '=', 'qualifications.idUser')
            ->join('programs', 'programs.id', '=', 'qualifications.idProgram')
            ->join('subjects', 'subjects.id', '=', 'qualifications.idSubject')
            ->join('groups', 'groups.id', '=', 'qualifications.idGroup')
            ->select('users.id', 'users.username as name', 'qualifications.average', 'qualifications.semesterNumber as semesterNumber', 'programs.nameProgram as nameProgram', 'subjects.name as nameSubject', 'groups.name as nameGroup')
            ->where('users.username', 'LIKE', '%' . $getValue . '%')
            ->orWhere('programs.nameProgram', 'LIKE', '%' . $getValue . '%')
            ->orWhere('subjects.name', 'LIKE', '%' . $getValue . '%')
            ->get();

        if (count($listAlumns) > 0) {
            $showClear = true;
        }
        return view('qualifications', compact('listAlumns', 'showClear', 'getValue', 'options', 'searchAlumn', 'searchProgram', 'searchSubject'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QualificationControllerController  $qualificationControllerController
     * @return \Illuminate\Http\Response
     */
    public function show(QualificationControllerController $qualificationController)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\QualificationControllerController  $qualificationController
     * @return \Illuminate\Http\Response
     */
    public function edit(QualificationControllerController $qualificationController)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\QualificationControllerController  $qualificationController
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QualificationControllerController $qualificationController)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QualificationControllerController  $qualificationController
     * @return \Illuminate\Http\Response
     */
    public function destroy(QualificationControllerController $qualificationController)
    {
        //
    }
    public function exportQualifications($type, $filter)
    {

        if ($type == 'null') {
            return (new QualificationsReport())->download('iuafCalificaciones.xlsx');
        } else {
            return (new QualificationsReport($type, $filter))->download('iuafCalificaciones.xlsx');
        }
    }
}
