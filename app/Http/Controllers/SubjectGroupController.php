<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PeoplesGroup;
use App\Models\Qualification;
use App\Models\Programs;
use App\Models\User;
use Illuminate\Support\Facades\DB;

use App\Exports\QualificationReport;

class SubjectGroupController extends Controller
{

    public function index(Request $request, $groupId, $subjectId)
    {

        $filterPeopleInClass = $request->get('findPeople');
        $clearPeople = false;
        $filterQualification = $request->get('findQualification');
        $clearQualification = false;

        $listGroups = DB::table('groups')
            ->join('groups_courses', 'groups_courses.idGroup', '=', 'groups.id')
            ->join('programs', 'programs.id', '=', 'groups.idProgram')
            ->join('subjects', 'groups_courses.idSubject', '=', 'subjects.id')
            ->select('groups.id', 'groups.name', 'groups.semesterNumber', 'programs.nameProgram as nameProgram', "subjects.name as nameSubject")
            ->where('groups.status', '=', 1)
            ->where('groups.id', '=', $groupId)
            ->where('subjects.id', '=', $subjectId)
            ->first();

        $peopleInClass = DB::table('peoples_group')
            ->join('users', "users.id", '=', 'peoples_group.idUser')
            ->where('peoples_group.idGroup', $groupId)
            ->where('peoples_group.idSubject', $subjectId)
            ->where('peoples_group.status', true)
            ->get();

        $getQualificationStudent = Qualification::select(
            'qualifications.id',
            'users.username AS name',
            'qualifications.assists',
            'qualifications.average'
        )
            ->join('users', 'users.id', '=', 'qualifications.idUser')
            ->where('status', true)
            ->get();

        $filteredTeacher = $peopleInClass->filter(function ($value, $key) {
            return $value->type == 'teacher';
        });
        $filteredTeacher->all();

        $filteredStudent = $peopleInClass->filter(function ($value, $key) {
            return $value->type == 'student';
        });
        $filteredStudent->all();

        $peopleInClass->sortByDesc('type');

        if ($filterPeopleInClass != '') {
            if ($filterPeopleInClass == '') {
                return view('material', compact('listGroups', 'filteredTeacher', 'filteredStudent', 'groupId', 'subjectId', 'peopleInClass', 'getQualificationStudent', 'filterPeopleInClass', 'clearPeople', 'filterQualification', 'clearQualification'));
            }

            $getDataPeople = DB::table('peoples_group')
                ->join('users', "users.id", '=', 'peoples_group.idUser')
                ->where('users.username', 'LIKE', '%' . $filterPeopleInClass . '%')
                ->where('peoples_group.idGroup', $groupId)
                ->where('peoples_group.idSubject', $subjectId)
                ->where('peoples_group.status', true)
                ->get();

            if (count($getDataPeople) > 0) {
                $peopleInClass = $getDataPeople;
                $clearPeople = true;
            }
        } else {
            if ($filterQualification == '') {
                return view('material', compact('listGroups', 'filteredTeacher', 'filteredStudent', 'groupId', 'subjectId', 'peopleInClass', 'getQualificationStudent', 'filterPeopleInClass', 'clearPeople', 'filterQualification', 'clearQualification'));
            }

            $getDataQualification = Qualification::select(
                'qualifications.id',
                'users.username AS name',
                'qualifications.assists',
                'qualifications.average'
            )
                ->join('users', 'users.id', '=', 'qualifications.idUser')
                ->where('users.username', 'LIKE', '%' . $filterQualification . '%')
                ->where("qualifications.status", true)
                ->get();

            if (count($getDataQualification) > 0) {
                $getQualificationStudent = $getDataQualification;
                $clearQualification = true;
            }
        }
        return view('material', compact('listGroups', 'filteredTeacher', 'filteredStudent', 'groupId', 'subjectId', 'peopleInClass', 'getQualificationStudent', 'filterPeopleInClass', 'clearPeople', 'filterQualification', 'clearQualification'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Subjects  $subjects
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $groupId)
    {
        $actualGroup = $groupId;
        if ($groupId != null || $groupId != 0) {
            $filter = $request->get('find');
            $showClear = false;

            $listSubject =  DB::table('groups_courses')
                ->join('groups', 'groups_courses.idGroup', '=', 'groups.id')
                ->join('programs', 'programs.id', '=', 'groups.idProgram')
                ->join('subjects', 'groups_courses.idSubject', '=', 'subjects.id')
                ->select('subjects.id', 'subjects.name', 'programs.nameProgram as nameProgram')
                ->where('subjects.status', '=', 1)
                ->where('groups_courses.status', '=', 1)
                ->where('groups.id', '=', $groupId)
                ->get();
            $teacherNames = DB::table('peoples_group')
                ->join('users', 'peoples_group.idUser', '=', 'users.id')
                ->select('users.username')
                ->where('peoples_group.idGroup', $groupId)
                ->where('peoples_group.type', "teacher")
                ->where('peoples_group.status', true)
                ->get();
            $studentList =  [];
            foreach ($listSubject as $suject) {
                $students  = DB::table('peoples_group')
                    ->join('groups', 'peoples_group.idGroup', '=', "groups.id")
                    ->where('peoples_group.idGroup', $groupId)
                    ->where('peoples_group.idSubject', $suject->id)
                    ->where('peoples_group.type', "student")
                    ->where('peoples_group.status', true)
                    ->count();

                array_push($studentList, $students);
            }

            if ($filter == '') {
                return view('SubjectsGroup', compact('listSubject', 'filter', 'showClear', "groupId", "studentList", "teacherNames"));
            }
            $listSubjectFiltered = DB::table('groups_courses')
                ->join('groups', 'groups_courses.idGroup', '=', 'groups.id')
                ->join('programs', 'programs.id', '=', 'groups.idProgram')
                ->join('subjects', 'groups_courses.idSubject', '=', 'subjects.id')
                ->select('subjects.id', 'subjects.name', 'programs.nameProgram as nameProgram')
                ->where('subjects.status', '=', 1)
                ->where('groups.id', '=', $groupId)
                ->where('subjects.name', 'LIKE', '%' . $filter . '%')
                ->orWhere('subjects.status', '=', 1)
                ->where('groups.id', '=', $groupId)
                ->where('programs.nameProgram', 'LIKE', '%' . $filter . '%')
                ->get();

            if (count($listSubjectFiltered) > 0) {
                $listSubject = $listSubjectFiltered;
                $showClear = true;
            }
            return view('SubjectsGroup', compact('listSubject', 'filter', 'showClear', 'groupId', "teacherNames", "studentList"));
        }
    }

    public function findUser()
    {
        $requestGroup = request()->except('_token');
        $result = User::select('username')
            ->where('username', 'LIKE', '%' . $requestGroup['text'] . '%')
            ->where('role', '=', $requestGroup['category'])
            ->orWhere('fullName', 'LIKE', '%' . $requestGroup['text'] . '%')
            ->where('role', '=', $requestGroup['category'])
            ->get();
        return $result;
    }
    public function agreePeopleinClassRoom()
    {
        $requestGroup = request()->except('_token');
        $findUser = User::select(
            'id',
            'role'
        )
            ->where('username', '=',  $requestGroup['userInClass'])
            ->orWhere('fullName', '=',  $requestGroup['userInClass'])
            ->first();

        $findProgram = Programs::select(
            'id'
        )
            ->where('nameProgram', '=',  $requestGroup['nameProgram'])
            ->first();

        $typeOfPeople = null;
        if ($findUser->role  == '2') {
            $typeOfPeople = 'student';
        } else {
            $typeOfPeople = 'teacher';
        }
        $existUserInClass = DB::table('peoples_group')
            ->where('idUser', $findUser->id)
            ->where('idGroup', $requestGroup['groupId'])
            ->where('idSubject',  $requestGroup['subjectId'])
            ->first();

        $result = null;
        if (empty($existUserInClass)) {
            $result =  DB::table('peoples_group')->insert(
                [
                    'idUser' => $findUser->id,
                    'idProgram' => $findProgram->id,
                    'semester' => $requestGroup['semester'],
                    'idGroup' => $requestGroup['groupId'],
                    'idSubject' => $requestGroup['subjectId'],
                    'type' => $typeOfPeople,
                    'status' => true
                ]
            );
        } else {

            $result =  DB::table('peoples_group')
                ->where('idUser', '=', $findUser->id)
                ->where('idGroup', $requestGroup['groupId'])
                ->where('idSubject',  $requestGroup['subjectId'])
                ->update(
                    [
                        'idProgram' => $findProgram->id,
                        'semester' => $requestGroup['semester'],
                        'type' => $typeOfPeople,
                        'status' => true
                    ]
                );
        }
        if ($findUser->role  == '2') {

            if (empty($existUserInClass)) {
                Qualification::insert([
                    'idUser' => $findUser->id,
                    'assists' => 0,
                    'average' => 0,
                    'semesterNumber' => $requestGroup['semester'],
                    'idProgram' =>  $findProgram->id,
                    'idGroup' => $requestGroup['groupId'],
                    'idSubject' => $requestGroup['subjectId'],
                    'status' => true
                ]);
            } else {
                DB::table('qualifications')
                    ->where('idUser', '=', $findUser->id)
                    ->where('idGroup', $requestGroup['groupId'])
                    ->where('idSubject',  $requestGroup['subjectId'])
                    ->update(
                        [
                            'status' => true
                        ]
                    );
            }
        }
        return $result;
    }

    public function removeInClass()
    {
        $requestGroup = request()->except('_token');

        $result =  DB::table('peoples_group')
            ->where('idUser', '=', $requestGroup['peopleInClass'])
            ->where('idGroup', $requestGroup['groupId'])
            ->where('idSubject',  $requestGroup['subjectId'])
            ->update(
                [
                    'status' => false
                ]
            );
        DB::table('qualifications')
            ->where('idUser', $requestGroup['peopleInClass'])
            ->where('idGroup', $requestGroup['groupId'])
            ->where('idSubject',  $requestGroup['subjectId'])
            ->update(
                [
                    'status' => false
                ]
            );
        return $result;
    }

    public function editQualification(Request $request)
    {
        $result = Qualification::select('id', 'assists', 'average')
            ->where('id', '=', $request['qualificationId'])
            ->first();

        return $result;
    }
    public function updateQualification(Request $request, $idQualification)
    {
        $requestQualification = request()->except('_token');
        $result = DB::table('qualifications')
            ->where('id', $idQualification)
            ->update(
                [
                    'assists' => $requestQualification['assists'],
                    'average' => $requestQualification['average']
                ]
            );
        return $result;
    }

    public function exportQualification($idSubject, $idGroup, $filter)
    {

        if ($filter == 'null') {
            return (new QualificationReport($idGroup, $idSubject))->download('iuafCalificacionesMateria.xlsx');
        } else {
            return (new QualificationReport($idGroup, $idSubject, $filter))->download('iuafCalificacionesMateria.xlsx');
        }
    }
}
