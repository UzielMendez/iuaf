<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Table extends Component
{  
    public $color;
    public $roleUser;
    public $listAcount;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    //public $roleUser;
    // public function __construct($roleActual)
    // {
    //     //
    //     $this->$roleUser = $roleActual;
    // }
    
    public function __construct($color,\Illuminate\Foundation\Application $roleActual,\Illuminate\Foundation\Application $listAcount)
    {
        //
        $this->color = $color;
        $this->roleUser = $roleActual; 
        $this->listAcount = $listAcount; 
        
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.table');
    }
}
