<?php

use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;

Route::get('/', [App\Http\Controllers\Auth\LoginController::class, 'showLoginView'])->middleware('guest');
Route::get('/classes', 'App\Http\Controllers\ClassesController@index');
Route::get('/academic', 'App\Http\Controllers\AcademicYearController@index');
Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('redirect', function () {
    alert()->info('Exito', 'try');
    return redirect('/home');
});

Route::get('/assignRoles', function () {
})->name('test');
Route::resource('library', App\Http\Controllers\BookLibraryController::class);
Route::patch('library/disabled/{id}', 'App\Http\Controllers\BookLibraryController@disableBooks');

Route::get('accounts/export/{type}/params/{param}/secondParms/{secondParam}', 'App\Http\Controllers\AccountStatusController@export')->name('accounts.exports');

Route::resource('accounts', App\Http\Controllers\AccountStatusController::class);

Route::view('/accountsAdmin', 'accountsAdmin');

Route::resource('bankMatters', App\Http\Controllers\SubjectsController::class);
Route::patch('bankMatters/disabled/{id}', 'App\Http\Controllers\SubjectsController@disableGroup');


Route::resource('groups', App\Http\Controllers\GroupController::class);
Route::patch('groups/disabled/{id}', 'App\Http\Controllers\GroupController@disableGroup');
Route::patch('groups/subject/disabled/{id}', 'App\Http\Controllers\GroupController@disableSubject');
Route::get('groups/find/subject', 'App\Http\Controllers\GroupController@findSubject');
Route::get('groups/{id}/subject', 'App\Http\Controllers\GroupController@findSubjectToProgram');

Route::get('subjectsGroup/{id}/subject', 'App\Http\Controllers\SubjectGroupController@edit');
Route::get('subjectsGroup/{id}/classRoom/subject/{idSubject}', 'App\Http\Controllers\SubjectGroupController@index');
Route::get('subjectsGroup/find/{id}/user', 'App\Http\Controllers\SubjectGroupController@findUser');
Route::post('subjectsGroup/agree/user', 'App\Http\Controllers\SubjectGroupController@agreePeopleinClassRoom');
Route::get('subjectsGroup/export/qualification', 'App\Http\Controllers\SubjectGroupController@exportQualification')->name('accounts.exports');
Route::patch('subjectsGroup/remove/people', 'App\Http\Controllers\SubjectGroupController@removeInClass');
Route::get('subjectsGroup/subjects/edit/qualification', 'App\Http\Controllers\SubjectGroupController@editQualification');
Route::patch('subjectsGroup/subjects/edit/{id}/qualification', 'App\Http\Controllers\SubjectGroupController@updateQualification');
Route::get('subjectsGroup/export/qualifications/subject/{subjectId}/group/{groupId}/{params}', 'App\Http\Controllers\SubjectGroupController@exportQualification')->name('subjectsGroup.exportsQualifications');

Route::view('/anthology', 'anthology');

Route::resource('qualifications', App\Http\Controllers\QualificationControllerController::class);
Route::get('qualifications/export/type/{type}/filter/{filter}', 'App\Http\Controllers\QualificationControllerController@exportQualifications');

if ($options['register'] ?? true) {
    Route::get('register', 'App\Http\Controllers\Auth\RegisterController@showRegistrationForm')->name('register');
}
